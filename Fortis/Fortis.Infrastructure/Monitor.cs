﻿using System;
using Microsoft.Extensions.Logging;

namespace Fortis.Infrastructure
{
    public class Monitor : IMonitor
    {
        // ----- Fields
        public static ILoggerFactory LoggerFactory {get;} = new LoggerFactory();
        private readonly ILogger _logger;

        // ----- Constructors
        public Monitor()
        {
            _logger = LoggerFactory.CreateLogger<Monitor>();
        }

        // ----- Public methods
        public IDisposable StartSession(string sessionName)
        {
            return new MonitoringSession(_logger, sessionName);
        }
    }
}