﻿using LinqToDB.Data;
using LinqToDB.DataProvider.PostgreSQL;

namespace Fortis.Infrastructure
{
    public class InfrastructureStartup
    {
        public static void Initialize(string connectionString)
        {
            // HACK : Linq2Db à besoin de charger les chaines de connexion au premier appel
            // dans une liste statique. Du fait d'un problème thread safe, il vaut mieux
            // remplir la liste au début, en appelant une méthode quelconque.
            DataConnection.AddConfiguration("Fortis", connectionString, new PostgreSQLDataProvider("PostgreSQL", PostgreSQLVersion.v93));
        }
    }
}