﻿using System;
using System.Diagnostics;
using Microsoft.Extensions.Logging;

namespace Fortis.Infrastructure
{
    public class MonitoringSession : IDisposable
    {
        // ----- Fields
        private readonly ILogger _logger;
        private readonly string _sessionName;
        private readonly Stopwatch _watch;

        // ----- Constructors
        public MonitoringSession(ILogger logger, string sessionName)
        {
            _logger = logger;
            _sessionName = sessionName;
            _watch = new Stopwatch();
            _watch.Start();
        }

        // ----- Public methods
        public void Dispose()
        {
            _watch.Stop();
            _logger.LogInformation($"{_sessionName} have been executed in {_watch.ElapsedMilliseconds} ms.");
        }
    }
}