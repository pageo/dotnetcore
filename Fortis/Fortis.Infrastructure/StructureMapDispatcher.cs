﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Fortis.Domain;
using StructureMap;

namespace Fortis.Infrastructure
{
    public class StructureMapDispatcher : IDispatcher
    {
        // ----- Fields
        private readonly IContainer _container;

        // ----- Constructors
        public StructureMapDispatcher(IContainer container)
        {
            _container = container;
        }

        // ----- Public methods
        public Task<TResult> Dispatch<TMessage, TResult>(TMessage message) where TMessage : class
        {
            return Task.Run(() => Handle<TMessage, TResult>(message));
        }
        public Task Dispatch<TMessage>(TMessage message) where TMessage : class
        {
            return Task.Run(() => Handle(message));
        }

        // ----- Internal logics
        private TResult Handle<TMessage, TResult>(TMessage message) where TMessage : class
        {
            var handler = GetHandler<TMessage, TResult>();
            return handler.Handle(message);
        }
        private void Handle<TMessage>(TMessage message) where TMessage : class
        {
            var handler = GetHandler<TMessage>();
            handler.Handle(message);
        }
        private IHandler<TMessage, TResult> GetHandler<TMessage, TResult>() where TMessage : class
        {
            var handlers = _container.GetAllInstances<IHandler<TMessage, TResult>>().ToArray();
            if (handlers.Length > 1)
                throw new Exception($"Multiple implementation of handler for the query '{typeof (TMessage).Name}'.");
            if (handlers.Length == 0)
                throw new Exception($"No implementation of handler for the query '{typeof (TMessage).Name}'. Missing register or invalid return type.");
            return handlers.Single();
        }
        private IHandler<TMessage> GetHandler<TMessage>() where TMessage : class
        {
            var handlers = _container.GetAllInstances<IHandler<TMessage>>().ToArray();
            if (handlers.Length > 1)
                throw new Exception($"Multiple implementation of handler for the query '{typeof (TMessage).Name}'.");
            if (handlers.Length == 0)
                throw new Exception($"No implementation of handler for the query '{typeof (TMessage).Name}'. Missing register or invalid return type.");
            return handlers.Single();
        }
    }
}