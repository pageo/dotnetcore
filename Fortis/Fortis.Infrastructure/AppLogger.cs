﻿using System;
using Fortis.Domain;
using NLog;

namespace Fortis.Infrastructure
{
    public class AppLogger : IAppLogger
    {
        // ----- Fields
        private static readonly ILogger NLogger = LogManager.GetCurrentClassLogger();

        // ----- Public methods
        public void Log(Exception exception)
        {
            NLogger.Error(exception.Message);
        }

        public void LogInfo(string message)
        {
            NLogger.Info(message);
        }

        public void LogWarning(string message)
        {
            NLogger.Warn(message);
        }
    }
}