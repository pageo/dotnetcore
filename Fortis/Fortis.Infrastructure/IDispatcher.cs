﻿using System.Threading.Tasks;

namespace Fortis.Infrastructure
{
    public interface IDispatcher
    {
        Task<TResult> Dispatch<TMessage, TResult>(TMessage message) where TMessage : class;
        Task Dispatch<TMessage>(TMessage message) where TMessage : class;
    }
}