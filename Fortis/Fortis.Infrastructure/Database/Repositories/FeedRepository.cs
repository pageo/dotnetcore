﻿using System.Linq;
using System.Collections.Generic;
using Fortis.Domain.Feeds;

namespace Fortis.Infrastructure.Database.Repositories
{
    public class FeedRepository : IFeedRepository
    {
        // ----- Fields
        private readonly IFortisConnection _connection;

        // ----- Constructors
        public FeedRepository(IFortisConnection connection)
        {
            _connection = connection;
        }

        // ----- Public methods
        public IEnumerable<Feed> GetAllFeeds()
        {
            return ( from feed in _connection.Feeds
                     select new Feed {
                         Id = feed.Id,
                         Name = feed.Name,
                         Image = feed.Image,
                         ProfilePic = feed.ProfilePic,
                         Status = feed.Status,
                         Url = feed.Url,
                         Hits = feed.Hits
                     } ).ToArray();
        }

        public Feed GetFeedById(int id)
        {
            return ( from feed in _connection.Feeds
                     where feed.Id == id
                     select new Feed {
                         Id = feed.Id,
                         Name = feed.Name,
                         Image = feed.Image,
                         ProfilePic = feed.ProfilePic,
                         Status = feed.Status,
                         Url = feed.Url,
                         Hits = feed.Hits
                     } ).FirstOrDefault();
        }
    }
}