﻿using System.Collections.Generic;
using Fortis.Domain.Assets;
using Fortis.Domain.WorkingFolders;

namespace Fortis.Infrastructure.Database.Repositories
{
    public class AssetRepository : IAssetRepository
    {
        // ----- Public methods
        public IReadOnlyCollection<AssetReadModel> Get(WorkingFolder workingFolder, int? take = null, int? skip = null)
        {
            return new[] {
                new AssetReadModel {Code = "12", RegionName = "LANGUEDOC", TownName = "CARCASSONNE", X = 43.2105327f, Y = 2.3150895f},
                new AssetReadModel {Code = "13", RegionName = "OCCITANIE", TownName = "BEZIER", X = 43.3481067f, Y = 3.1992012f},
                new AssetReadModel {Code = "14", RegionName = "RHONE", TownName = "LYON", X = 45.7576946f, Y = 4.8038767f},
                new AssetReadModel {Code = "15", RegionName = "LANGUEDOC", TownName = "MONTPELLIER", X = 43.6100166f, Y = 3.8391422f},
                new AssetReadModel {Code = "16", RegionName = "PROVENCE-COTE-AZUR", TownName = "NICE", X = 43.7030575f, Y = 7.2179151f},
            };
        }
    }
}