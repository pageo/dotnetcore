﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fortis.Domain;
using Fortis.Domain.Users;
using Fortis.Domain.WorkingFolders;
using Fortis.Infrastructure.Database.Tables;
using LinqToDB;

namespace Fortis.Infrastructure.Database.Repositories
{
    public class WorkingFolderRepository : IWorkingFolderRepository
    {
        // ----- Fields
        private readonly IFortisConnection _connection;
        private readonly IClock _clock;

        // ----- Constructors
        public WorkingFolderRepository(
            IFortisConnection connection,
            IClock clock)
        {
            _connection = connection;
            _clock = clock;
        }

        // ----- Public methods
        public IEnumerable<WorkingFolderListItem> GetByUserId(int userId)
        {
            var query = from workingFolder in _connection.WorkingFolders
                        join user in _connection.Users on userId equals user.Id
                        where user.IsAdmin || workingFolder.UserId == userId
                        select ConvertToWorkingFolderReadModel(workingFolder, user);
            return query.ToArray();
        }
        public WorkingFolder Get(int workingFolderId)
        {
            var query = from workingFolder in _connection.WorkingFolders
                        join user in _connection.Users on workingFolder.UserId equals user.Id
                        where workingFolder.Id == workingFolderId
                        select new { workingFolder, user };

            var data = query.FirstOrDefault();
            return data == null ? null : ConvertToWorkingFolder(data.workingFolder, data.user);
        }
        public long Create(WorkingFolder workingFolder)
        {
            var table = new TableWorkingFolder {
                Name = workingFolder.Name,
                CreationDate = _clock.Now(),
                UserId = workingFolder.Owner
            };
            var result = _connection.Insert(table);
            return (long)result;
        }
        public bool Exists(string workingFolderName, long userId)
        {
            return _connection.WorkingFolders.Any(x => x.Name == workingFolderName && x.UserId == userId);
        }
        public bool Exists(int workingFolderId)
        {
            return _connection.WorkingFolders.Any(x => x.Id == workingFolderId);
        }
        public WorkingFolderDetails GetReadModel(int workingFolderId)
        {
            return (from workingFolder in _connection.WorkingFolders
                    join owner in _connection.Users on workingFolder.UserId equals owner.Id
                    where workingFolder.Id == workingFolderId
                    select new WorkingFolderDetails {
                        Id = workingFolder.Id,
                        Name = workingFolder.Name
                    }
                ).FirstOrDefault();
        }
        public void Save(int userId, WorkingFolder workingFolder)
        {
            if (workingFolder == null) throw new ArgumentNullException(nameof(workingFolder));
        }
        public void Delete(int workingFolderId)
        {
            _connection.WorkingFolders
                               .Where(x => x.Id == workingFolderId)
                               .Delete();
        }

        // ----- Internal logics
        private static WorkingFolder ConvertToWorkingFolder(
            TableWorkingFolder tableWorkingFolder,
            TableUser tableUser)
        {
            var owner = new User {
                Id = tableUser.Id,
                IsAdmin = tableUser.IsAdmin,
                LastLoginDate = tableUser.LastLoginDate
            };

            return new WorkingFolder(
                tableWorkingFolder.Id,
                tableWorkingFolder.Name,
                owner.Id);
        }
        private static WorkingFolderListItem ConvertToWorkingFolderReadModel(TableWorkingFolder workingFolder, TableUser user)
        {
            return new WorkingFolderListItem {
                Id = workingFolder.Id,
                Name = workingFolder.Name,
                CreationDate = workingFolder.CreationDate,
                IsOwner = workingFolder.UserId == user.Id
            };
        }
    }
}