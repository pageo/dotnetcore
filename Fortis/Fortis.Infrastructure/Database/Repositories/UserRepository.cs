﻿using System;
using System.Linq;
using Fortis.Domain.Users;
using Fortis.Infrastructure.Database.Tables;
using LinqToDB;

namespace Fortis.Infrastructure.Database.Repositories
{
    public class UserRepository : IUserRepository
    {
        // ----- Fields
        private readonly IFortisConnection _connection;

        // ----- Constructors
        public UserRepository(IFortisConnection connection)
        {
            _connection = connection;
        }

        // ----- Public methods
        public User GetUser(string loginOrEmail)
        {
            var query = from user in _connection.Users
                        where user.Login.ToUpper() == loginOrEmail.ToUpper()
                              || (user.Email != null && user.Email.ToUpper() == loginOrEmail.ToUpper())
                        select new User {
                            Id = user.Id,
                            Login = user.Login,
                            FullName = user.FullName,
                            Email = user.Email,
                            Avatar = user.Avatar,
                            LastLoginDate = user.LastLoginDate,
                            IsAdmin = user.IsAdmin
                        };

            return query.FirstOrDefault();
        }

        public User GetUser(int userId)
        {
            return ( from user in _connection.Users
                     where user.Id == userId
                     select new User {
                         Id = user.Id,
                         Login = user.Login,
                         FullName = user.FullName,
                         Email = user.Email,
                         Avatar = user.Avatar,
                         LastLoginDate = user.LastLoginDate,
                         IsAdmin = user.IsAdmin
                     } ).FirstOrDefault();
        }

        public bool Exists(int userId)
        {
            return _connection.Users.Any(x => x.Id == userId);
        }

        public bool Exists(string loginOrEmail)
        {
            return _connection.Users.Any(x => x.Login.ToUpper() == loginOrEmail.ToUpper() || x.Email.ToUpper() == loginOrEmail.ToUpper());
        }

        public int Create(User user)
        {
            var tableUser = new TableUser {
                Id = user.Id,
                Login = user.Login,
                FullName = user.FullName,
                Email = user.Email,
                Password = user.Password,
                Avatar = user.Avatar,
                LastLoginDate = user.LastLoginDate
            };
            var result = _connection.Insert(tableUser);

            return Convert.ToInt32(result);
        }

        public void UpdateLastLoginDate(int userId, DateTime lastLoginDate)
        {
            _connection.Users.Where(x => x.Id == userId).Set(x => x.LastLoginDate, lastLoginDate).Update();
        }
    }
}