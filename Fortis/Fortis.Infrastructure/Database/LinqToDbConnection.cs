﻿using System.Collections.Generic;
using System.Linq;
using Fortis.Infrastructure.Database.Tables;
using LinqToDB;
using LinqToDB.Data;

namespace Fortis.Infrastructure.Database
{
    public class LinqToDbConnection : DataConnection, IFortisConnection
    {
        // ----- Constructors
        public LinqToDbConnection() : base("Fortis")
        {
            CommandTimeout = 60000;
        }

        // ----- DbSets
        public IQueryable<TableUser> Users => GetTable<TableUser>();
        public IQueryable<TableFeed> Feeds => GetTable<TableFeed>();
        public IQueryable<TableWorkingFolder> WorkingFolders => GetTable<TableWorkingFolder>();

        // ----- Public methods
        object IFortisConnection.Insert<T>(T table)
        {
            return this.InsertWithIdentity(table);
        }

        public long InsertRange<T>(IEnumerable<T> elements)
        {
            var result = this.BulkCopy(elements);
            return result.RowsCopied;
        }

        void IFortisConnection.Update<T>(T table)
        {
            this.Update(table);
        }

        void IFortisConnection.BeginTransaction()
        {
            this.BeginTransaction();
        }
    }
}