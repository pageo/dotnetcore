﻿using System;
using LinqToDB.Mapping;

namespace Fortis.Infrastructure.Database.Tables
{
    [Table(Schema = "fortis", Name = "WorkingFolder")]
    public class TableWorkingFolder
    {
        [PrimaryKey, Identity]
        public int Id { get; set; }

        [Column, NotNull]
        public string Name { get; set; }

        [Column, NotNull]
        public DateTime CreationDate { get; set; }

        [Column, NotNull]
        public int UserId { get; set; }
    }
}
