﻿using LinqToDB.Mapping;

namespace Fortis.Infrastructure.Database.Tables
{
    [Table(Schema = "fortis", Name = "Feed")]
    public class TableFeed
    {
        [PrimaryKey, Identity]
        public int Id { get; set; }

        [Column, NotNull]
        public string Name { get; set; }

        [Column, NotNull]
        public string Image { get; set; }

        [Column]
        public string Status { get; set; }

        [Column]
        public string ProfilePic { get; set; }

        [Column, NotNull]
        public string Url { get; set; }

        [Column, NotNull]
        public int Hits { get; set; }
    }
}