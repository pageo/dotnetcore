﻿using System;
using LinqToDB.Mapping;

namespace Fortis.Infrastructure.Database.Tables
{
    [Table(Schema = "fortis", Name = "User")]
    public class TableUser
    {
        [PrimaryKey, Identity]
        public int Id { get; set; }

        [Column]
        public string Login { get; set; }

        [Column]
        public string Email { get; set; }

        [Column, NotNull]
        public string Password { get; set; }

        [Column]
        public string FullName { get; set; }

        [Column]
        public string Avatar { get; set; }

        [Column, NotNull]
        public DateTime LastLoginDate { get; set; }

        [Column, NotNull]
        public bool IsAdmin { get; set; }
    }
}