﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fortis.Infrastructure.Database.Tables;

namespace Fortis.Infrastructure.Database
{
    public interface IFortisConnection : IDisposable
    {
        IQueryable<TableUser> Users { get; }
        IQueryable<TableFeed> Feeds { get; }
        IQueryable<TableWorkingFolder> WorkingFolders { get; }

        object Insert<T>(T table) where T : class;
        long InsertRange<T>(IEnumerable<T> elements);
        void Update<T>(T table);
        void BeginTransaction();
        void CommitTransaction();
        void RollbackTransaction();
    }
}