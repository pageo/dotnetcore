﻿using System;

namespace Fortis.Infrastructure
{
    public interface IMonitor
    {
        IDisposable StartSession(string sessionName);
    }
}