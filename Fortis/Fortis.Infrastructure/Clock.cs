﻿using System;
using Fortis.Domain;

namespace Fortis.Infrastructure
{
    public class Clock : IClock
    {
        // ----- Public methods
        public DateTime Now()
        {
            return DateTime.Now;
        }
    }
}