﻿using System;
using FluentAssertions;
using Fortis.Pdp.Common;
using Fortis.Pdp.Feeds;
using Fortis.Pdp.Models;
using Fortis.Pdp.Utils;
using NSubstitute;
using Xunit;

namespace Fortis.Tests.PdpTests
{
    public class FeedsBuilderTests
    {
        // ----- Fields
        private readonly FeedsBuilder _builder;
        private readonly PdpParameters _parameters;

        // ----- Constructors
        public FeedsBuilderTests()
        {
            _parameters = Substitute.For<PdpParameters>();
            var logger = Substitute.For<AppLogger>();

            _builder = new FeedsBuilder(_parameters, logger);
        }

        // ----- Tests
        [Fact]
        public void build_an_feed_with_data_properties()
        {
            var data = new Data {
                Infos = new[] {
                    new Info {
                        Name = "National Geographic Channel",
                        ProfilePic = "http://api.androidhive.info/feed/img/nat.jpg",
                        Hits = 12,
                        Image = "http://api.androidhive.info/feed/img/cosmos.jpg",
                        Status = "Science is a beautiful and emotional human endeavor"
                    }
                }
            };

            var feeds = _builder.Build(data);

            feeds.Should().HaveCount(1);
            feeds[0].ShouldBeEquivalentTo(new Feed {
                Name = "National Geographic Channel",
                Image = "http://api.androidhive.info/feed/img/cosmos.jpg",
                Status = "Science is a beautiful and emotional human endeavor",
                ProfilePic = "http://api.androidhive.info/feed/img/nat.jpg",
                Hits = 12,
            });
        }

        [Fact(Skip = "should ignore data if already processed")]
        public void ignore_datas_if_already_processed()
        {
            _parameters.LastUpdateDateHandled = new DateTime(2016, 3, 3);
            var data = new Data {
                Infos = new[] {
                    new Info {
                        Name = "National Geographic Channel",
                        ProfilePic = "http://api.androidhive.info/feed/img/nat.jpg",
                        Hits = 12,
                        Image = "http://api.androidhive.info/feed/img/cosmos.jpg",
                        Status = "Science is a beautiful and emotional human endeavor"
                    }
                }
            };

            var feeds = _builder.Build(data);

            feeds.Should().BeEmpty();
        }
    }
}