﻿using System;
using FluentAssertions;
using Fortis.Pdp.Utils;
using Xunit;

namespace Fortis.Tests.PdpTests
{
    public class PeriodTests
    {
        // ----- Tests
        [Fact]
        public void throw_error_when_creating_period_with_start_date_after_the_end_date()
        {
            Action action = () => new Period(new DateTime(2016, 1, 1), new DateTime(2015, 1, 1));

            action.ShouldThrowExactly<ArgumentException>()
                .WithMessage("The start date should be before the end date to create a valid period.");
        }

        [Fact]
        public void indicate_if_period_contains_another()
        {
            var bigPeriod = new Period(new DateTime(2016, 1, 1), new DateTime(2016, 12, 1));
            var shortPeriod = new Period(new DateTime(2016, 6, 1), new DateTime(2016, 6, 2));

            bigPeriod.Contains(shortPeriod).Should().BeTrue();
        }

        [Fact]
        public void indicate_that_separated_periods_are_not_contained()
        {
            var period1 = new Period(new DateTime(2016, 1, 1), new DateTime(2016, 6, 1));
            var period2 = new Period(new DateTime(2016, 7, 1), new DateTime(2016, 12, 1));

            period1.Contains(period2).Should().BeFalse();
        }

        [Fact]
        public void a_no_end_period_contains_sub_period()
        {
            var noEndPeriod = new Period(new DateTime(2016, 1, 1), DateTime.MaxValue);
            var shortPeriod = new Period(new DateTime(2016, 7, 1), new DateTime(2016, 12, 1));

            noEndPeriod.Contains(shortPeriod).Should().BeTrue();
        }

        [Fact]
        public void an_period_is_not_contains_in_a_crossed_period()
        {
            var period = new Period(new DateTime(2016, 1, 1), new DateTime(2016, 10, 1));
            var crossedPeriod = new Period(new DateTime(2016, 7, 1), new DateTime(2016, 12, 1));

            period.Contains(crossedPeriod).Should().BeFalse();
        }

        [Fact]
        public void a_no_end_period_contains_another_smaller_no_end_period()
        {
            var bigPeriod = new Period(new DateTime(2016, 1, 1), DateTime.MaxValue);
            var shortPeriod = new Period(new DateTime(2016, 7, 1), DateTime.MaxValue);

            bigPeriod.Contains(shortPeriod).Should().BeTrue();
        }

        [Fact]
        public void should_indicate_if_date_is_contained()
        {
            var date = new DateTime(2016, 2, 1);
            var period = new Period(new DateTime(2016, 1, 1), new DateTime(2016, 6, 1));

            period.Contains(date).Should().BeTrue();
        }

        [Fact]
        public void should_indicate_if_a_period_contains_a_date()
        {
            var date = new DateTime(2016, 10, 1);
            var period = new Period(new DateTime(2016, 1, 1), new DateTime(2016, 6, 1));

            period.Contains(date).Should().BeFalse();
        }

        [Fact]
        public void should_indicate_date_is_contained_in_a_not_ended_period()
        {
            var date = new DateTime(2016, 2, 1);
            var period = new Period(new DateTime(2016, 1, 1), DateTime.MaxValue);

            period.Contains(date).Should().BeTrue();
        }

        [Fact]
        public void define_duration()
        {
            var start = new DateTime(2016, 2, 1);
            var end = new DateTime(2016, 2, 23, 10, 0, 0);
            var period = new Period(start, end);

            period.Duration.ShouldBeEquivalentTo(new TimeSpan(22, 10, 0, 0));
        }

        [Fact]
        public void intercept_other_period()
        {
            var period1 = new Period(new DateTime(2014, 12, 31), DateTime.MaxValue);
            var period2 = new Period(new DateTime(2016, 5, 25), DateTime.MaxValue);

            period1.Contains(period2).Should().BeTrue();
        }
    }
}