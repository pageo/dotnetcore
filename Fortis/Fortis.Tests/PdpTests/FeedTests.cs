﻿using FluentAssertions;
using Fortis.Pdp.Feeds;
using Fortis.Pdp.Utils;
using Xunit;

namespace Fortis.Tests.PdpTests
{
    public class FeedTests
    {
        [Fact]
        public void clone_into_new_object()
        {
            var feed = new Feed {
                Name = "1",
                Status = "titi"
            };

            var clone = feed.Clone();

            clone.Should().NotBe(feed);
            clone.ShouldBeEquivalentTo(feed);
        }
    }
}
