﻿using System.IO;
using FluentAssertions;
using Fortis.Pdp.Feeds;
using Xunit;

namespace Fortis.Tests.PdpTests
{
    public class FeedsParserTests
    {
        // ----- Fields
        private readonly FeedsParser _parser;

        // ----- Constructors
        public FeedsParserTests()
        {
            var filePath = Path.Combine("Resources", "feeds.xml");
            _parser = new FeedsParser(filePath);
        }

        // ----- Tests
        [Fact(Skip = "Pb de resources")]
        public void parse_the_correct_number_of_site()
        {
            // Acts
            var data1 = _parser.ParseNextData();
            var data2 = _parser.ParseNextData();
            var data3 = _parser.ParseNextData();
            var data4 = _parser.ParseNextData();

            // Asserts
            data1.Should().NotBeNull();
            data2.Should().BeNull();
            data3.Should().BeNull();
            data4.Should().BeNull();
        }

        [Fact(Skip = "Pb de resources")]
        public void parse_properties_correctly()
        {
            // Acts
            var data = _parser.ParseNextData();

            // Asserts
            data.Should().NotBeNull();
            data.Infos.Should().NotBeNull().And.HaveCount(2);
            data.Infos[0].Name.ShouldBeEquivalentTo("National Geographic Channel");
            data.Infos[0].Hits.ShouldBeEquivalentTo(12);
            data.Infos[1].Name.ShouldBeEquivalentTo("TIME");
            data.Infos[1].Hits.ShouldBeEquivalentTo(13);
        }

        [Fact(Skip = "Pb de resources")]
        public void have_0_data_parsing_when_empty_file()
        {
            // Arranges
            var parser = new FeedsParser(@".\Resources\feeds_empty.xml");

            // Acts
            var data = parser.ParseNextData();

            // Asserts
            data.Should().BeNull();
        }
    }
}
