﻿using System;
using FluentAssertions;
using Fortis.Domain;
using Fortis.Domain.Users;
using Fortis.Domain.Users.Exceptions;
using NSubstitute;
using Xunit;

namespace Fortis.Tests.DomainTests
{
    public class AuthenticateUserCommandHandlerTests
    {
        // ----- Fields
        private readonly AuthenticateUserCommandHandler _handler;
        private User _user;

        // ----- Constructors
        public AuthenticateUserCommandHandlerTests()
        {
            var clock = Substitute.For<IClock>();
            var userRepository = Substitute.For<IUserRepository>();
            userRepository.GetUser(Arg.Do<string>(x => { if (_user != null) _user.Login = x; }))
                          .Returns(info => _user);

            _handler = new AuthenticateUserCommandHandler(userRepository, clock);
        }

        // ----- Tests
        [Fact]
        public void authenticate_user()
        {
            // Arranges
            _user = new User {Id = 11, IsAdmin = true};

            // Acts
            var command = new AuthenticateUserCommand("login", "password");
            var token = _handler.Handle(command);

            // Asserts
            token.ShouldBeEquivalentTo(_user.Id);
        }

        [Fact]
        public void throw_exception_if_user_is_not_found()
        {
            // Acts
            var command = new AuthenticateUserCommand("login", "password");
            Action act = () => _handler.Handle(command);

            // Asserts
            act.ShouldThrow<UserNotFound>().WithMessage("The user with login 'login' was not found.");
        }
    }
}