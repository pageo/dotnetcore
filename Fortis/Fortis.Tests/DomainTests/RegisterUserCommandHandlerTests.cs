﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Fortis.Domain.Users;
using Fortis.Domain.Users.Exceptions;
using NSubstitute;
using Xunit;

namespace Fortis.Tests.DomainTests
{
    public class RegisterUserCommandHandlerTests
    {
        // ----- Fields
        private readonly RegisterUserCommandHandler _handler;
        private readonly List<User> _users = new List<User>();
        private User _user;

        // ----- Constructors
        public RegisterUserCommandHandlerTests()
        {
            var userRepository = Substitute.For<IUserRepository>();
            userRepository.Create(Arg.Do<User>(x => _user = x)).Returns(info => _user != null ? 1 : 0);
            userRepository.Exists(Arg.Do<string>(x => { _user = new User {Login = x}; })).Returns(info => _users.Any(x => x.Login == _user.Login));

            _handler = new RegisterUserCommandHandler(userRepository);
        }

        // ----- Tests
        [Fact]
        public void register_user()
        {
            // Arranges
            var command = new RegisterUserCommand("Olivier Page", "opage", "password");

            // Acts
            var result = _handler.Handle(command);

            // Asserts
            result.Should().Be(1);
        }

        [Fact]
        public void throw_exception_if_user_already_exist()
        {
            // Arranges
            _users.Add(new User {Login = "opage"});

            // Acts
            var command = new RegisterUserCommand("Olivier Page", "opage", "password");
            Action act = () => _handler.Handle(command);

            // Asserts
            act.ShouldThrow<UserWithSameNameAlreadyExists>().WithMessage("The user with login/email 'opage' already exist.");
        }
    }
}