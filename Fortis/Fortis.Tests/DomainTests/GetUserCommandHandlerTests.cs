﻿using FluentAssertions;
using Fortis.Domain.Users;
using NSubstitute;
using Xunit;

namespace Fortis.Tests.DomainTests
{
    public class GetUserCommandHandlerTests
    {
        // ----- Fields
        private readonly GetUserCommandHandler _handler;
        private User _user;

        // ----- Constructors
        public GetUserCommandHandlerTests()
        {
            var userRepository = Substitute.For<IUserRepository>();
            userRepository.GetUser(Arg.Do<int>(x => _user.Id = x)).Returns(info => _user);

            _handler = new GetUserCommandHandler(userRepository);
        }

        // ----- Tests
        [Fact]
        public void get_user()
        {
            // Arranges
            _user = new User {Id = 11, IsAdmin = true};

            // Acts
            var command = new GetUserCommand(11);
            var user = _handler.Handle(command);

            // Asserts
            user.ShouldBeEquivalentTo(_user);
        }
    }
}