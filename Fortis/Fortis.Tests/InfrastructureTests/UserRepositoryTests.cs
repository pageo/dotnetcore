﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Fortis.Infrastructure.Database;
using Fortis.Infrastructure.Database.Repositories;
using Fortis.Infrastructure.Database.Tables;
using NSubstitute;
using Xunit;

namespace Fortis.Tests.InfrastructureTests
{
    public class UserRepositoryTests
    {
        // ----- Fields
        private readonly List<TableUser> _users = new List<TableUser>();
        private readonly UserRepository _repository;

        // ----- Constructors
        public UserRepositoryTests()
        {
            var fortisConnection = Substitute.For<IFortisConnection>();
            fortisConnection.Users.Returns(_users.AsQueryable());

            _repository = new UserRepository(fortisConnection);
        }

        // ----- Tests
        [Fact]
        public void get_user_by_id()
        {
            // Arranges
            _users.AddRange(new[] {
                new TableUser {Id = 11, Login = "Olivier", IsAdmin = true, LastLoginDate = new DateTime(2016, 12, 3)},
                new TableUser {Id = 33, Login = "Florentina", IsAdmin = false, LastLoginDate = new DateTime(2016, 12, 3)},
            });

            // Acts
            var user = _repository.GetUser(11);

            // Asserts
            user.ShouldBeEquivalentTo(new {Id = 11, Login = "Olivier", IsAdmin = true, LastLoginDate = new DateTime(2016, 12, 3)}, options =>
                    options.ExcludingMissingMembers());
        }

        [Fact]
        public void get_user_by_name()
        {
            // Arranges
            _users.AddRange(new[] {
                new TableUser {Id = 11, Login = "Olivier", IsAdmin = true, LastLoginDate = new DateTime(2016, 12, 3)},
                new TableUser {Id = 33, Login = "Florentina", IsAdmin = false, LastLoginDate = new DateTime(2016, 12, 3)},
            });

            // Acts
            var user = _repository.GetUser("Florentina");

            // Asserts
            user.ShouldBeEquivalentTo(new {Id = 33, Login = "Florentina", IsAdmin = false, LastLoginDate = new DateTime(2016, 12, 3)}, options =>
                    options.ExcludingMissingMembers());
        }
    }
}