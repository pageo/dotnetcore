﻿using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Fortis.Infrastructure.Database;
using Fortis.Infrastructure.Database.Repositories;
using Fortis.Infrastructure.Database.Tables;
using NSubstitute;
using Xunit;

namespace Fortis.Tests.InfrastructureTests
{
    public class FeedRepositoryTests
    {
        // ----- Fields
        private readonly List<TableFeed> _feeds = new List<TableFeed>();
        private readonly FeedRepository _repository;

        // ----- Constructors
        public FeedRepositoryTests()
        {
            var fortisConnection = Substitute.For<IFortisConnection>();
            fortisConnection.Feeds.Returns(_feeds.AsQueryable());

            _repository = new FeedRepository(fortisConnection);
        }

        // ----- Tests
        [Fact]
        public void get_feed_by_id()
        {
            // Arranges
            _feeds.AddRange(new[] {
                new TableFeed {
                    Id = 1,
                    Name = "National Geographic Channel",
                    Image = "http://api.androidhive.info/feed/img/cosmos.jpg",
                    Status =
                        "\"Science is a beautiful and emotional human endeavor,\" says Brannon Braga, executive producer and director. \"And Cosmos is all about making science an experience.\"",
                    ProfilePic = "http://api.androidhive.info/feed/img/nat.jpg",
                    Hits = 12
                },
                new TableFeed {
                    Id = 2,
                    Name = "TIME",
                    Image = "http://api.androidhive.info/feed/img/time_best.jpg",
                    Status = "30 years of Cirque du Soleil's best photos",
                    ProfilePic = "http://api.androidhive.info/feed/img/time.png",
                    Url = "http://ti.me/1qW8MLB",
                    Hits = 13
                }
            });

            // Acts
            var feed = _repository.GetFeedById(2);

            // Asserts
            feed.ShouldBeEquivalentTo(
                new {
                    Id = 2,
                    Name = "TIME",
                    Image = "http://api.androidhive.info/feed/img/time_best.jpg",
                    Status = "30 years of Cirque du Soleil's best photos",
                    ProfilePic = "http://api.androidhive.info/feed/img/time.png",
                    Url = "http://ti.me/1qW8MLB",
                    Hits = 13
                });
        }

        [Fact]
        public void get_all_feeds()
        {
            // Arranges
            _feeds.AddRange(new[] {
                new TableFeed {
                    Id = 1,
                    Name = "National Geographic Channel",
                    Image = "http://api.androidhive.info/feed/img/cosmos.jpg",
                    Status =
                        "\"Science is a beautiful and emotional human endeavor,\" says Brannon Braga, executive producer and director. \"And Cosmos is all about making science an experience.\"",
                    ProfilePic = "http://api.androidhive.info/feed/img/nat.jpg",
                    Hits = 12
                },
                new TableFeed {
                    Id = 2,
                    Name = "TIME",
                    Image = "http://api.androidhive.info/feed/img/time_best.jpg",
                    Status = "30 years of Cirque du Soleil's best photos",
                    ProfilePic = "http://api.androidhive.info/feed/img/time.png",
                    Url = "http://ti.me/1qW8MLB",
                    Hits = 13
                }
            });

            // Acts
            var feeds = _repository.GetAllFeeds();

            // Asserts
            feeds.Should().HaveCount(_feeds.Count);
        }
    }
}