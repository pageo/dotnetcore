using System;
using System.IO.Compression;
using Fortis.CommonHost.Core.Filters;
using Fortis.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;
using NLog.Extensions.Logging;
using StructureMap;
using Swashbuckle.Swagger.Model;

namespace Fortis.CommonHost.Core
{
    public class Startup
    {
        // ----- Fields
        public IConfigurationRoot Configuration { get; }

        // ----- Constructors
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsEnvironment("Development"))
                builder.AddApplicationInsightsSettings(developerMode: true);

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
            InfrastructureStartup.Initialize(Configuration.GetConnectionString("Fortis"));
        }

        // ----- Public methods
        // This method gets called by the runtime. Use this method to add services to the container
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddApplicationInsightsTelemetry(Configuration);

            // Add Compression
            services.Configure<GzipCompressionProviderOptions>
                (options => options.Level = CompressionLevel.Fastest);
            services.AddResponseCompression(options => {
                options.Providers.Add<GzipCompressionProvider>();
            });

            // Add MVC
            services.AddMvc()
                    .AddMvcOptions(opts => {
                        opts.FormatterMappings.SetMediaTypeMappingForFormat("xml", new MediaTypeHeaderValue("application/xml"));
                        opts.FormatterMappings.SetMediaTypeMappingForFormat("json", new MediaTypeHeaderValue("application/json"));
                        opts.RespectBrowserAcceptHeader = true;
                        opts.ReturnHttpNotAcceptable = true;
                    });

            // Add Swagger
            services.AddSwaggerGen(options => {
                options.MultipleApiVersions(new[] {
                    new Info {
                        Version = "v2",
                        Title = "Fortis API",
                        Description = "ASP.NET Core Web API Fortis",
                        TermsOfService = "None",
                        Contact = new Contact {Name = "Olivier Page", Email = "opage4@gmail.com", Url = "http://github.com/opage"}
                    },
                    new Info {
                        Version = "v1",
                        Title = "Fortis API",
                        Description = "ASP.NET Core Web API Fortis",
                        TermsOfService = "None",
                        Contact = new Contact {Name = "Olivier Page", Email = "opage4@gmail.com", Url = "http://github.com/opage"}
                    }
                }, (description, version) => description.RelativePath.Contains($"api/{version}"));
                options.OperationFilter<TokenHeaderParameterOperationFilter>();
            });

            // Configure IoC
            return ConfigureIoC(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            //add NLog to ASP.NET Core
            loggerFactory.AddNLog();

            //needed for non-NETSTANDARD platforms: configure nlog.config in your project root
            loggerFactory.ConfigureNLog("nlog.config");

#pragma warning disable 612, 618

            app.UseApplicationInsightsRequestTelemetry();
            app.UseApplicationInsightsExceptionTelemetry();

#pragma warning restore 612, 618

            // Enable static files middleware.
            //app.UseStaticFiles();

            // Diagnostics
            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseExceptionHandler("/error");

            // Compression
            app.UseResponseCompression();

            // Enable Mvc
            app.UseMvc();

            // Enable middleware to serve generated Swagger as a JSON endpoint
            app.UseSwagger();

            // Enable middleware to serve swagger-ui assets (HTML, JS, CSS etc.)
            app.UseSwaggerUi();

            // Reverse-proxy server other than IIS
            app.UseForwardedHeaders(new ForwardedHeadersOptions {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });
        }

        public IServiceProvider ConfigureIoC(IServiceCollection services)
        {
            var container = DependencyConfig.Register();
            container.Configure(config => {
                // Register stuff in container, using the StructureMap APIs...
                config.Scan(_ => {
                    _.AssemblyContainingType(typeof(Startup));
                    _.WithDefaultConventions();
                });

                //Populate the container using the service collection
                config.Populate(services);
            });

            return container.GetInstance<IServiceProvider>();
        }
    }
}