﻿using System.Linq;
using System.Net;
using System.Security.Principal;
using Fortis.CommonHost.Core.Filters.Exceptions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;

namespace Fortis.CommonHost.Core.Filters
{
    public class HeaderAuthenticationFilter : ActionFilterAttribute
    {
        // ----- Public methods
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            try {
                var identity = Authenticate(context);
                if (identity == null) {
                    RejectRequest(context, "Missing token");
                    return;
                }
                context.HttpContext.User = new GenericPrincipal(identity, null);
            }
            catch (InvalidToken ex) {
                RejectRequest(context, ex.Message);
            }
        }

        // ----- Internal logics
        private static BasicAuthenticationIdentity Authenticate(ActionExecutingContext context)
        {
            StringValues tokenValues;
            var headers = context.HttpContext.Request.Headers;
            if (!headers.TryGetValue("Token", out tokenValues)) return null;
            var tokenValue = tokenValues.FirstOrDefault();
            var token = Token.Parse(tokenValue);
            return new BasicAuthenticationIdentity(token.UserId);
        }

        private static void RejectRequest(ActionExecutingContext context, string message)
        {
            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            context.Result = new JsonResult(JsonConvert.SerializeObject(new { Message = message }));
        }
    }
}