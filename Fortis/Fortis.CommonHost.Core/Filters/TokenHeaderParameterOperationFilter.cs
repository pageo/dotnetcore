﻿using System.Linq;
using System.Collections.Generic;
using Swashbuckle.Swagger.Model;
using Swashbuckle.SwaggerGen.Generator;

namespace Fortis.CommonHost.Core.Filters
{
    public class TokenHeaderParameterOperationFilter : IOperationFilter
    {
        // ----- Public methods
        public void Apply(Operation operation, OperationFilterContext context)
        {
            var filterPipeline = context.ApiDescription.ActionDescriptor.FilterDescriptors;
            var isAuthorized = filterPipeline.Select(filterInfo => filterInfo.Filter).Any(filter => filter is HeaderAuthenticationFilter);

            if (!isAuthorized) return;
            if (operation.Parameters == null) operation.Parameters = new List<IParameter>();

            operation.Parameters.Add(new NonBodyParameter { Name = "Token", In = "header", Description = "token", Required = false, Type = "string" });
        }
    }
}
