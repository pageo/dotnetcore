﻿using System;
using System.Linq;
using System.Text;
using Fortis.CommonHost.Core.Filters.Exceptions;
using Newtonsoft.Json;

namespace Fortis.CommonHost.Core.Filters
{
    public class Token
    {
        // ----- Constants
        private const string FieldSeparator = ";";
        private const string ValueSeparator = ":";
        private const string IdProperty = "id";
        private const string CreationDateProperty = "creationDate";

        // ----- Properties
        public int UserId { get; }
        public DateTime CreationDate { get; private set; }

        // ----- Constructors
        public Token(int userId)
        {
            UserId = userId;
            CreationDate = DateTime.Now;
        }

        // ----- Overrides
        public override string ToString()
        {
            var values = new[] {
                $"{IdProperty}{ValueSeparator}{UserId}",
                $"{CreationDateProperty}{ValueSeparator}{CreationDate}"
            };
            var fullText = string.Join(FieldSeparator, values);
            var bytes = Encoding.UTF8.GetBytes(fullText);
            return JsonConvert.SerializeObject(Convert.ToBase64String(bytes));
        }

        // ----- Public methods
        public static Token Parse(string base64Value)
        {
            try
            {
                var bytes = Convert.FromBase64String(base64Value);
                var value = Encoding.UTF8.GetString(bytes);
                var fields = value.Split(new[] { FieldSeparator }, StringSplitOptions.None);
                var id = GetValue(IdProperty, fields[0]);
                var creationDate = GetValue(CreationDateProperty, fields[1]);
                return new Token(int.Parse(id)) {
                    CreationDate = DateTime.Parse(creationDate)
                };
            }
            catch (Exception ex) {
                throw new InvalidToken(ex);
            }
        }

        // ----- Utils
        private static string GetValue(string property, string value)
        {
            return value.Split(new[] { property + ValueSeparator }, StringSplitOptions.None).Last();
        }
    }
}