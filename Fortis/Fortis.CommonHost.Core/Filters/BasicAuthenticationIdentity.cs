﻿using System.Security.Principal;

namespace Fortis.CommonHost.Core.Filters
{
    public class BasicAuthenticationIdentity : GenericIdentity
    {
        // ----- Properties
        public int Id { get; }

        // ----- Constructors
        public BasicAuthenticationIdentity(int id)
            : base(id.ToString(), "Basic")
        {
            Id = id;
        }
    }
}