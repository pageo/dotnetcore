﻿using System;

namespace Fortis.CommonHost.Core.Filters.Exceptions
{
    public class InvalidToken : Exception
    {
        public InvalidToken(Exception exception): base("Invalid token", exception) { }
    }
}