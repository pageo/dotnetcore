﻿namespace Fortis.CommonHost.Core.Models
{
    public class UserModel
    {
        // ----- Properties
        public string Name { get; set; }
        public int Age { get; set; }
    }
}