﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Fortis.CommonHost.Core.Controllers.Base;
using Fortis.Domain.Animes;
using Fortis.Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace Fortis.CommonHost.Core.Controllers
{
    [Route("api/v1")]
    public class AnimesController : CustomController
    {
        // ----- Fields
        private readonly IDispatcher _dispatcher;

        // ----- Constructors
        public AnimesController(IDispatcher dispatcher)
        {
            _dispatcher = dispatcher;
        }

        // ----- Public methods
        [HttpGet("animes")]
        public async Task<IActionResult> Get()
        {
            var command = new GetAnimesCommand();
            return Ok(await _dispatcher.Dispatch<GetAnimesCommand, IEnumerable<AnimeReadModel>>(command));
        }
    }
}
