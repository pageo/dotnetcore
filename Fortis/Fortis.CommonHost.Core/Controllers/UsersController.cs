﻿using System.Threading.Tasks;
using Fortis.CommonHost.Core.Controllers.Base;
using Fortis.CommonHost.Core.Filters;
using Fortis.Domain.Users;
using Fortis.Domain.Users.Exceptions;
using Fortis.Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace Fortis.CommonHost.Core.Controllers
{
    [Route("api/v1")]
    public class UsersController : CustomController
    {
        // ----- Fields
        private readonly IDispatcher _dispatcher;

        // ----- Constructors
        public UsersController(IDispatcher dispatcher)
        {
            _dispatcher = dispatcher;
        }

        // ----- Publics methods
        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate([FromBody] AuthenticateUserCommand command)
        {
            try {
                if (!ModelState.IsValid) return BadRequest(ModelState);

                var userId = await _dispatcher.Dispatch<AuthenticateUserCommand, int>(command);
                var token = new Token(userId);
                return Ok(token.ToString());
            }
            catch (UserNotFound) {
                return Unauthorized();
            }
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] RegisterUserCommand command)
        {
            try {
                if (!ModelState.IsValid) return BadRequest(ModelState);

                var userId = await _dispatcher.Dispatch<RegisterUserCommand, int>(command);
                var token = new Token(userId);
                return Ok(token.ToString());
            }
            catch (UserWithSameNameAlreadyExists) {
                // Conflict
                return StatusCode(409);
            }
        }

        [HttpGet("user")]
        [HeaderAuthenticationFilter]
        public async Task<IActionResult> Get()
        {
            var command = new GetUserCommand(User.Id);
            return Ok(await _dispatcher.Dispatch<GetUserCommand, User>(command));
        }
    }
}