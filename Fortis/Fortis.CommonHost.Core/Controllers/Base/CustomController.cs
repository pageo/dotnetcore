﻿using Fortis.CommonHost.Core.Filters;
using Microsoft.AspNetCore.Mvc;

namespace Fortis.CommonHost.Core.Controllers.Base
{
    public class CustomController : Controller
    {
        // ----- Fields
        public new BasicAuthenticationIdentity User => (BasicAuthenticationIdentity) base.User.Identity;
    }
}