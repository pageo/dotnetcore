﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Fortis.CommonHost.Core.Controllers.Base;
using Fortis.CommonHost.Core.Filters;
using Fortis.Domain.Assets;
using Fortis.Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace Fortis.CommonHost.Core.Controllers
{
    [HeaderAuthenticationFilter]
    [Route("api/v1")]
    public class AssetsController : CustomController
    {
        // ----- Fields
        private readonly IDispatcher _dispatcher;

        // ----- Constructors
        public AssetsController(IDispatcher dispatcher)
        {
            _dispatcher = dispatcher;
        }

        // ----- Public methods
        [HttpGet("workingfolders/{id:int}/assets")]
        public async Task<IActionResult> GetAssets(int id, int? skip, int? take)
        {
            var query = new GetAssetByWorkingFolderId(id, User.Id, skip, take);
            var result = await _dispatcher.Dispatch<GetAssetByWorkingFolderId, IEnumerable<AssetReadModel>>(query);
            return Ok(result);
        }
    }
}
