﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Fortis.CommonHost.Core.Controllers.Base;
using Fortis.CommonHost.Core.Filters;
using Fortis.Domain.WorkingFolders;
using Fortis.Domain.WorkingFolders.Exceptions;
using Fortis.Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace Fortis.CommonHost.Core.Controllers
{
    [HeaderAuthenticationFilter]
    [Route("api/v1")]
    public class WorkingFoldersController : CustomController
    {
        // ----- Fields
        private readonly IDispatcher _dispatcher;

        // ----- Constructors
        public WorkingFoldersController(IDispatcher dispatcher)
        {
            _dispatcher = dispatcher;
        }

        // ----- Public methods
        [HttpGet("workingfolders")]
        public async Task<IActionResult> Get()
        {
            var query = new GetAllWorkingFoldersQuery(User.Id);
            var result = await _dispatcher.Dispatch<GetAllWorkingFoldersQuery, IEnumerable<WorkingFolderListItem>>(query);
            return Ok(result);
        }

        [HttpGet("workingfolders/{id:int}")]
        public async Task<IActionResult> Get(int id)
        {
            var query = new GetWorkingFolderDetailsQuery(User.Id, id);
            var workingFolder = await _dispatcher.Dispatch<GetWorkingFolderDetailsQuery, WorkingFolderDetails>(query);
            return Ok(workingFolder);
        }

        [HttpPost("workingfolders", Name = "createWorkingFolder")]
        public async Task<IActionResult> Post([FromBody] CreateWorkingFolderParameters parameters)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);

                var command = new CreateNewWorkingFolderCommand(parameters.Name, User.Id);
                var createdWorkingFolderId = await _dispatcher.Dispatch<CreateNewWorkingFolderCommand, long>(command);
                return CreatedAtRoute("createWorkingFolder", new { id = createdWorkingFolderId }, new { id = createdWorkingFolderId });
            }
            catch (WorkingFolderWithSameNameAlreadyExists) {
                // Conflict
                return StatusCode(409);
            }
        }

        [HttpPut("workingfolders/{id:int}")]
        public async Task<IActionResult> Put(int id, [FromBody] UpdateWorkingFolderParameters parameters)
        {
            var command = new SaveWorkingFolderCommand(User.Id, id);
            await _dispatcher.Dispatch(command);
            return Ok();
        }

        [HttpDelete("workingfolders/{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            var command = new DeleteWorkingFolderCommand(User.Id, id);
            await _dispatcher.Dispatch(command);
            return Ok();
        }
    }
}
