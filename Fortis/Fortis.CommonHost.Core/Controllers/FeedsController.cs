﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Fortis.CommonHost.Core.Controllers.Base;
using Fortis.CommonHost.Core.Filters;
using Fortis.Domain.Feeds;
using Fortis.Domain.Users.Exceptions;
using Fortis.Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace Fortis.CommonHost.Core.Controllers
{
    [HeaderAuthenticationFilter]
    [Route("api/v1")]
    public class FeedsController : CustomController
    {
        // ----- Fields
        private readonly IDispatcher _dispatcher;

        // ----- Constructors
        public FeedsController(IDispatcher dispatcher)
        {
            _dispatcher = dispatcher;
        }

        // ----- Publics methods
        [HttpGet("feeds")]
        public async Task<IActionResult> Get()
        {
            try {
                var command = new GetFeedsCommand(User.Id);
                var result = await _dispatcher.Dispatch<GetFeedsCommand, IEnumerable<Feed>>(command);
                return Ok(result);
            }
            catch (UserNotFound) {
                return Unauthorized();
            }
        }
    }
}