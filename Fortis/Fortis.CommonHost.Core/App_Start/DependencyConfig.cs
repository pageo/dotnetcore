﻿using StructureMap;

namespace Fortis.CommonHost.Core
{
    public class DependencyConfig
    {
        // ----- Public methods
        public static IContainer Register()
        {
            var bootstrapStartup = new Bootstrap.Startup();
            return bootstrapStartup.CreateContainer();
        }
    }
}