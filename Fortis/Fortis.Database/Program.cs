﻿using System;
using System.Diagnostics;
using System.Reflection;
using CommandLine;
using CommandLine.Text;
using FluentMigrator;
using FluentMigrator.Runner;
using FluentMigrator.Runner.Announcers;
using FluentMigrator.Runner.Initialization;

namespace Fortis.Database
{
    class Program
    {
        private static void Main(string[] args)
        {
            var options = new CommandLineOptions();
            if (Parser.Default.ParseArguments(args, options)) {
                if (options.IsPreview)
                    MigrateDownAndUpInPreview(options.ConnectionString, 1);
                else
                    MigrateToLatest(options.ConnectionString);
            }
        }

        private static void MigrateToLatest(string connectionString)
        {
            Console.WriteLine("- Start of the database migration to latest version.");
            Console.WriteLine("\t-Connection string: " + connectionString);

            RunMigration(connectionString, runner => { runner.MigrateUp(true); });

            Console.WriteLine("- End of the database migration");
        }

        private static void MigrateDownAndUpInPreview(string connectionString, int targetVersion)
        {
            Console.WriteLine(
                $"- Start of the preview database migration to {targetVersion} and then to latest version.");
            Console.WriteLine("\t-Connection string: " + connectionString);

            RunMigration(connectionString, runner =>
            {
                runner.MigrateUp(true);
                runner.MigrateDown(targetVersion, true);
                runner.MigrateUp(true);
            }, true);

            Console.WriteLine("- End of the preview database migration");
        }

        private static void RunMigration(string connectionString, Action<MigrationRunner> action, bool isPreview = false)
        {
            if (action == null) throw new ArgumentNullException(nameof(action));

            var announcer = new TextWriterAnnouncer(s => Debug.WriteLine(s));
            var assembly = Assembly.GetExecutingAssembly();
            var migrationContext = new RunnerContext(announcer);
            var options = new MigrationOptions {PreviewOnly = isPreview, Timeout = 3600};
            var factory = new FluentMigrator.Runner.Processors.Postgres.PostgresProcessorFactory();
            using (var processor = factory.Create(connectionString, announcer, options))
            {
                var runner = new MigrationRunner(assembly, migrationContext, processor);
                action(runner);
            }
        }

        public class MigrationOptions : IMigrationProcessorOptions
        {
            public bool PreviewOnly { get; set; }
            public string ProviderSwitches { get; set; }
            public int Timeout { get; set; }
        }
    }

    public class CommandLineOptions
    {
        [Option('c', "connectionString", Required = true, HelpText = "Défini la chaîne de connection à utiliser.")]
        public string ConnectionString { get; set; }

        [Option("preview", DefaultValue = false, HelpText = "Execute an attempt to migrate but does not commit.")]
        public bool IsPreview { get; set; }

        [ParserState]
        public IParserState LastParserState { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this,
                (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }
}