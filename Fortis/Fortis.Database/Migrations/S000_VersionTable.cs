﻿using FluentMigrator.VersionTableInfo;

namespace Fortis.Database.Migrations
{
    [VersionTableMetaData]
    public class S000_VersionTable : IVersionTableMetaData
    {
        public string TableName => "_Version";
        public string ColumnName => "Version";
        public string DescriptionColumnName => "Description";
        public bool OwnsSchema => true;
        public string SchemaName => "fortis";
        public string UniqueIndexName => "UC_Version";

        public virtual string AppliedOnColumnName => "AppliedOn";
        public object ApplicationContext { get; set; }
    }
}
