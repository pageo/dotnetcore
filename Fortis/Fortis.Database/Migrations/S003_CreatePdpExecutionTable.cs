﻿using FluentMigrator;

namespace Fortis.Database.Migrations
{
    [Migration(3, "Creation of the PdpExecution table")]
    public class S003_CreatePdpExecutionTable : Migration
    {
        public override void Up()
        {
            Create
                .Table("PdpExecution").WithDescription("Table de rapports d'exécution du PDP Fortis.").InSchema("fortis")
                .WithColumn("Id").AsInt32().Identity().WithColumnDescription("Id technique.")
                .WithColumn("StartDate").AsDateTime().NotNullable().WithColumnDescription("Start date of the pdp execution.")
                .WithColumn("EndDate").AsDateTime().NotNullable().WithColumnDescription("End date of the pdp execution.")
                .WithColumn("LastUpdateDateHandled").AsDateTime().Nullable().WithColumnDescription("The last update date handled by the pdp.")
                .WithColumn("InsertedCount").AsInt32().NotNullable().WithColumnDescription("Inserted count during the pdp execution.")
                .WithColumn("DeletedCount").AsInt32().NotNullable().WithColumnDescription("Deleted count during the pdp execution.")
                .WithColumn("RejectedCount").AsInt32().NotNullable().WithColumnDescription("Rejected count during the pdp execution.")
                .WithColumn("Type").AsString(50).NotNullable().WithColumnDescription("Type.");

            Create
                .PrimaryKey("PK_PdpExecution")
                .OnTable("PdpExecution").WithSchema("fortis")
                .Column("Id");
        }

        public override void Down()
        {
            Delete
                .PrimaryKey("PK_PdpExecution")
                .FromTable("PdpExecution").InSchema("fortis");

            Delete
                .Table("PdpExecution").InSchema("fortis");
        }
    }
}
