﻿using FluentMigrator;

namespace Fortis.Database.Migrations
{
    [Migration(2, "Create of Feed Table")]
    public class S002_CreateOfFeedTable : Migration
    {
        public override void Up()
        {
            Create
                .Table("Feed").InSchema("fortis")
                .WithColumn("Id").AsInt32().Identity().NotNullable().WithColumnDescription("Identifiant technique auto-incrémentable.")
                .WithColumn("Name").AsString().NotNullable().WithColumnDescription("Nom du feed.")
                .WithColumn("Image").AsString().NotNullable().WithColumnDescription("Image du feed.")
                .WithColumn("Status").AsString().WithColumnDescription("Status du feed.")
                .WithColumn("ProfilePic").AsString().WithColumnDescription("Profil image du feed.")
                .WithColumn("Url").AsString().NotNullable().WithColumnDescription("Url du feed.")
                .WithColumn("Hits").AsInt32().WithDefaultValue(0).WithColumnDescription("Nombre de hits du feed.")
                .WithColumn("ModificationDate").AsDateTime().NotNullable().WithColumnDescription("Date de dernière modification date.");

            Create
                .PrimaryKey("PK_Feed")
                .OnTable("Feed").WithSchema("fortis")
                .Column("Id");
        }

        public override void Down()
        {
            Delete
                .Table("Feed").InSchema("fortis");
        }
    }
}