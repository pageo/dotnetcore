﻿using FluentMigrator;

namespace Fortis.Database.Migrations
{
    [Migration(5, "Create Asset Table.")]
    public class S005_CreateOfAssetTable : Migration
    {
        public override void Up()
        {
            Create
                .Table("Asset")
                .WithDescription("Table regroupant toutes les faces issues de Géméo et historisées de -6mois -> +1an.")
                .InSchema("fortis")
                .WithColumn("Id").AsInt64().PrimaryKey("PK_Asset").Identity().WithColumnDescription("Identifiant technique auto-incrémenté.")
                .WithColumn("Code").AsString(30).NotNullable().WithColumnDescription("Code")
                .WithColumn("TownName").AsString(40).Nullable().WithColumnDescription("Nom de la commune")
                .WithColumn("RegionName").AsString(30).Nullable().WithColumnDescription("Nom de la région.")
                .WithColumn("X").AsFloat().Nullable().WithColumnDescription("Position en X (ou longitude)")
                .WithColumn("Y").AsFloat().Nullable().WithColumnDescription("Position en Y (ou latitude)");
        }

        public override void Down()
        {
            Delete
                .Table("Asset")
                .InSchema("fortis");
        }
    }
}
