﻿using FluentMigrator;

namespace Fortis.Database.Migrations
{
    [Migration(1, "Create of User Table")]
    public class S001_CreateOfUserTable : Migration
    {
        public override void Up()
        {
            Create
                .Table("User").InSchema("fortis")
                .WithColumn("Id").AsInt32().Identity().NotNullable().WithColumnDescription("Identifiant technique auto-incrémentable.")
                .WithColumn("Login").AsString(100).NotNullable().WithColumnDescription("Login de l'utilisateur")
                .WithColumn("Password").AsString().NotNullable().WithColumnDescription("Mot de passe de l'utilisateur")
                .WithColumn("Email").AsString(100).Nullable().WithColumnDescription("Email de l'utilisateur")
                .WithColumn("FullName").AsString().Nullable().WithColumnDescription("Nom complet de l'utilisateur")
                .WithColumn("Avatar").AsString().Nullable().WithColumnDescription("Avatar de l'utilisateur")
                .WithColumn("LastLoginDate").AsDateTime().NotNullable().WithColumnDescription("Date de dernière connexion de l'utilisateur")
                .WithColumn("IsAdmin").AsBoolean().WithDefaultValue(false).WithColumnDescription("Indique si l'utilisateur est un administrateur.");

            Create
                .PrimaryKey("PK_User")
                .OnTable("User").WithSchema("fortis")
                .Column("Id");
        }

        public override void Down()
        {
            Delete
                .Table("User").InSchema("fortis");
        }
    }
}