﻿using FluentMigrator;

namespace Fortis.Database.Migrations
{
    [Migration(4, "Create WorkingFolder Table.")]
    public class S004_CreateOfWorkingFolderTable : Migration
    {
        public override void Up()
        {
            Create
                .Table("WorkingFolder")
                .WithDescription("Table des dossiers de travail des utilisateurs.")
                .InSchema("fortis")
                .WithColumn("Id").AsInt32().Identity().WithColumnDescription("Identifiant technique d'un dossier.")
                .WithColumn("Name").AsString(50).NotNullable().WithColumnDescription("Nom du dossier.")
                .WithColumn("CreationDate").AsDateTime().Nullable().WithColumnDescription("Date de création du dossier.")
                .WithColumn("UserId").AsInt32().NotNullable().WithDefaultValue(0).WithColumnDescription("Identifiant de l'utilisateur propriétaire du dossier.");

            Create
                .PrimaryKey("PK_WorkingFolder")
                .OnTable("WorkingFolder")
                .WithSchema("fortis")
                .Column("Id");

            Create
                .ForeignKey("FK_WorkingFolder_User")
                .FromTable("WorkingFolder").InSchema("fortis").ForeignColumn("UserId")
                .ToTable("User").InSchema("fortis").PrimaryColumn("Id");
        }

        public override void Down()
        {
            Delete
                .ForeignKey("FK_WorkingFolder_User")
                .OnTable("WorkingFolder").InSchema("fortis");

            Delete
                .Table("WorkingFolder")
                .InSchema("fortis");
        }
    }
}
