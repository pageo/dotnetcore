﻿using StructureMap;

namespace Fortis.Bootstrap
{
    public class Startup
    {
        // ----- Public methods
        public IContainer CreateContainer()
        {
            var container = new Container();
            container.Configure(expression => {
                expression.AddRegistry(new DomainRegistry());
                expression.AddRegistry(new InfrastructureRegistry());
            });
            return container;
        }
    }
}