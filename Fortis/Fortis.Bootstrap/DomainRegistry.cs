﻿using System.Reflection;
using Fortis.Bootstrap.Utils;
using Fortis.Domain;
using Fortis.Domain.Validators;
using StructureMap;
using StructureMap.TypeRules;

namespace Fortis.Bootstrap
{
    public class DomainRegistry : Registry
    {
        // ----- Constructors
        public DomainRegistry()
        {
            Scan(scanner => {
                scanner.Assembly(typeof (IHandler<,>).GetAssembly());
                scanner.AddAllTypesOf(typeof (IHandler<,>));
                scanner.WithDefaultConventions();
                scanner.With(new GenericDecorateConvention(typeof(IHandler<,>), typeof(AuthorizationHandler<,>), type => typeof(IAuthenticable).IsAssignableFrom(type.GetGenericArguments()[0])));
                scanner.With(new GenericDecorateConvention(typeof (IHandler<,>), typeof (MonitorableCommandHandler<,>)));
                scanner.With(new GenericDecorateConvention(typeof (IHandler<,>), typeof (LoggableCommandHandler<,>)));
                scanner.With(new GenericDecorateConvention(typeof (IHandler<,>), typeof (TransactionalCommandHandler<,>)));
            });

            Scan(scanner => {
                scanner.Assembly(typeof(IHandler<>).GetAssembly());
                scanner.AddAllTypesOf(typeof(IHandler<>));
                scanner.WithDefaultConventions();
                scanner.With(new GenericDecorateConvention(typeof(IHandler<>), typeof(AuthorizationHandler<>), type => typeof(IAuthenticable).IsAssignableFrom(type.GetGenericArguments()[0])));
                scanner.With(new GenericDecorateConvention(typeof(IHandler<>), typeof(MonitorableCommandHandler<>)));
                scanner.With(new GenericDecorateConvention(typeof(IHandler<>), typeof(LoggableCommandHandler<>)));
                scanner.With(new GenericDecorateConvention(typeof(IHandler<>), typeof(TransactionalCommandHandler<>)));
            });

            Scan(scanner => {
                scanner.ConnectImplementationsToTypesClosing(typeof(IValidator<>));
            });

            For(typeof(IValidator<>)).Add(typeof(DefaultValidator<>));
        }
    }
}