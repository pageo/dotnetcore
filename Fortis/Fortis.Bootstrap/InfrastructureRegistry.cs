﻿using Fortis.Domain;
using Fortis.Domain.Assets;
using Fortis.Domain.Feeds;
using Fortis.Domain.Users;
using Fortis.Domain.WorkingFolders;
using Fortis.Infrastructure;
using Fortis.Infrastructure.Database;
using Fortis.Infrastructure.Database.Repositories;
using StructureMap;

namespace Fortis.Bootstrap
{
    public class InfrastructureRegistry : Registry
    {
        // ----- Constructors
        public InfrastructureRegistry()
        {
            // ----- Utils
            ForSingletonOf<IAppLogger>().Use<AppLogger>();
            ForSingletonOf<IMonitor>().Use<Monitor>();
            For<IDispatcher>().Use<StructureMapDispatcher>();
            For<IClock>().Use(new Clock());

            // ----- Repositories
            ForSingletonOf<IFortisConnection>().Use<LinqToDbConnection>().Transient();
            For<IUserRepository>().Use<UserRepository>();
            For<IFeedRepository>().Use<FeedRepository>();
            For<IWorkingFolderRepository>().Use<WorkingFolderRepository>();
            For<IAssetRepository>().Use<AssetRepository>();
        }
    }
}