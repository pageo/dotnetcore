﻿using System;
using Fortis.Domain;

namespace Fortis.Bootstrap.Utils
{
    public class LoggableCommandHandler<TMessage, TResult> : IHandler<TMessage, TResult> where TMessage : class
    {
        // ----- Fields
        private readonly IHandler<TMessage, TResult> _decoratedHandler;
        private readonly IAppLogger _logger;

        // ----- Constructors
        public LoggableCommandHandler(IHandler<TMessage, TResult> decoratedHandler, IAppLogger logger)
        {
            _decoratedHandler = decoratedHandler;
            _logger = logger;
        }

        // ----- Public methods
        public TResult Handle(TMessage command)
        {
            try {
                return _decoratedHandler.Handle(command);
            }
            catch (Exception ex) {
                _logger.Log(ex);
                throw;
            }
        }
    }

    public class LoggableCommandHandler<TMessage> : IHandler<TMessage> where TMessage : class
    {
        // ----- Fields
        private readonly IHandler<TMessage> _decoratedHandler;
        private readonly IAppLogger _logger;

        // ----- Constructors
        public LoggableCommandHandler(IHandler<TMessage> decoratedHandler, IAppLogger logger)
        {
            _decoratedHandler = decoratedHandler;
            _logger = logger;
        }

        // ----- Public methods
        public void Handle(TMessage command)
        {
            try {
                _decoratedHandler.Handle(command);
            }
            catch (Exception ex) {
                _logger.Log(ex);
                throw;
            }
        }
    }
}