﻿using System;
using System.Reflection;
using StructureMap;
using StructureMap.Graph;
using StructureMap.Graph.Scanning;

namespace Fortis.Bootstrap.Utils
{
    public class GenericDecorateConvention : IRegistrationConvention
    {
        // ----- Fields
        private readonly Type _typeToDecorate;
        private readonly Type _decoratorType;
        private readonly Predicate<Type> _predicate;

        // ----- Constructors
        public GenericDecorateConvention(Type typeToDecorate, Type decoratorType, Predicate<Type> predicate = null)
        {
            _typeToDecorate = typeToDecorate;
            _decoratorType = decoratorType;
            _predicate = predicate;
        }

        // ----- Public methods
        public void ScanTypes(TypeSet types, Registry registry)
        {
            foreach (var type in types.AllTypes()) {
                var interfacesImplemented = type.GetInterfaces();

                foreach (var interfaceImplemented in interfacesImplemented) {
                    if (interfaceImplemented.IsConstructedGenericType && interfaceImplemented.GetGenericTypeDefinition() == _typeToDecorate && (_predicate == null || _predicate(interfaceImplemented))) {
                        var genericParameters = interfaceImplemented.GetGenericArguments();
                        var decoratorType = _decoratorType.MakeGenericType(genericParameters);
                        registry.For(interfaceImplemented).DecorateAllWith(decoratorType);
                    }
                }
            }
        }
    }
}