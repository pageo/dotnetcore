﻿using Fortis.Domain;
using Fortis.Infrastructure.Database;
using Npgsql;

namespace Fortis.Bootstrap.Utils
{
    public class TransactionalCommandHandler<TMessage, TResult> : IHandler<TMessage, TResult> where TMessage : class
    {
        // ----- Fields
        private readonly IHandler<TMessage, TResult> _decoratedHandler;
        private readonly IFortisConnection _connection;

        // ----- Constructors
        public TransactionalCommandHandler(IHandler<TMessage, TResult> decoratedHandler, IFortisConnection connection)
        {
            _decoratedHandler = decoratedHandler;
            _connection = connection;
        }

        // ----- Public methods
        public TResult Handle(TMessage command)
        {
            try {
                _connection.BeginTransaction();
                var result = _decoratedHandler.Handle(command);
                _connection.CommitTransaction();
                return result;
            }
            catch (PostgresException) {
                _connection.RollbackTransaction();
                throw;
            }
            finally {
                _connection.Dispose();
            }
        }
    }

    public class TransactionalCommandHandler<TMessage> : IHandler<TMessage> where TMessage : class
    {
        // ----- Fields
        private readonly IHandler<TMessage> _decoratedHandler;
        private readonly IFortisConnection _connection;

        // ----- Constructors
        public TransactionalCommandHandler(IHandler<TMessage> decoratedHandler, IFortisConnection connection)
        {
            _decoratedHandler = decoratedHandler;
            _connection = connection;
        }

        // ----- Public methods
        public void Handle(TMessage command)
        {
            try {
                _connection.BeginTransaction();
                _decoratedHandler.Handle(command);
                _connection.CommitTransaction();
            }
            catch (PostgresException) {
                _connection.RollbackTransaction();
                throw;
            }
            finally {
                _connection.Dispose();
            }
        }
    }
}