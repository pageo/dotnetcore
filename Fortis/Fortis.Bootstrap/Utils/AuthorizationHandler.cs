﻿using Fortis.Domain;
using Fortis.Domain.Users;
using Fortis.Domain.Users.Exceptions;

namespace Fortis.Bootstrap.Utils
{
    public class AuthorizationHandler<TMessage, TResult> : IHandler<TMessage, TResult> where TMessage : class, IAuthenticable
    {
        // ----- Fields
        private readonly IUserRepository _userRepository;
        private readonly IHandler<TMessage, TResult> _decoratedHandler;

        // ----- Constructors
        public AuthorizationHandler(IUserRepository userRepository, IHandler<TMessage, TResult> decoratedHandler)
        {
            _userRepository = userRepository;
            _decoratedHandler = decoratedHandler;
        }

        // ----- Public methods
        public TResult Handle(TMessage message)
        {
            var exists = _userRepository.Exists(message.UserId);
            if (!exists)
                throw new UserNotFound(message.UserId);
            return _decoratedHandler.Handle(message);
        }
    }

    public class AuthorizationHandler<TMessage> : IHandler<TMessage> where TMessage : class, IAuthenticable
    {
        // ----- Fields
        private readonly IUserRepository _userRepository;
        private readonly IHandler<TMessage> _decoratedHandler;

        // ----- Constructors
        public AuthorizationHandler(IUserRepository userRepository, IHandler<TMessage> decoratedHandler)
        {
            _userRepository = userRepository;
            _decoratedHandler = decoratedHandler;
        }

        // ----- Public methods
        public void Handle(TMessage message)
        {
            var exists = _userRepository.Exists(message.UserId);
            if (!exists)
                throw new UserNotFound(message.UserId);
            _decoratedHandler.Handle(message);
        }
    }
}
