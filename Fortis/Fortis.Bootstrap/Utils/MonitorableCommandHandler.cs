﻿using Fortis.Domain;
using Fortis.Infrastructure;

namespace Fortis.Bootstrap.Utils
{
    public class MonitorableCommandHandler<TMessage, TResult> : IHandler<TMessage, TResult> where TMessage : class
    {
        // ----- Fields
        private readonly IHandler<TMessage, TResult> _decoratedHandler;
        private readonly IMonitor _monitor;

        // ----- Constructors
        public MonitorableCommandHandler(IHandler<TMessage, TResult> decoratedHandler, IMonitor monitor)
        {
            _decoratedHandler = decoratedHandler;
            _monitor = monitor;
        }

        // ----- Public methods
        public TResult Handle(TMessage command)
        {
            var sessionName = $"{_decoratedHandler.GetType().Name}.Handle()";
            using (_monitor.StartSession(sessionName)) {
                return _decoratedHandler.Handle(command);
            }
        }
    }

    public class MonitorableCommandHandler<TMessage> : IHandler<TMessage> where TMessage : class
    {
        // ----- Fields
        private readonly IHandler<TMessage> _decoratedHandler;
        private readonly IMonitor _monitor;

        // ----- Constructors
        public MonitorableCommandHandler(IHandler<TMessage> decoratedHandler, IMonitor monitor)
        {
            _decoratedHandler = decoratedHandler;
            _monitor = monitor;
        }

        // ----- Public methods
        public void Handle(TMessage command)
        {
            var sessionName = $"{_decoratedHandler.GetType().Name}.Handle()";
            using (_monitor.StartSession(sessionName)) {
                _decoratedHandler.Handle(command);
            }
        }
    }
}