﻿using Fortis.Pdp.Common;
using Fortis.Pdp.Database;
using Fortis.Pdp.Feeds;
using Fortis.Pdp.Utils;
using Microsoft.Extensions.Configuration;
using StructureMap;

namespace Fortis.Pdp
{
    public class InversionOfControl
    {
        // ----- Fields
        public static InversionOfControl Instance { get; } = new InversionOfControl();

        private readonly Container _container;

        // ----- Constructors
        public InversionOfControl()
        {
            _container = new Container(configuration =>
            {
                // Configuration
                configuration.ForSingletonOf<IConfigurationBuilder>().Use(new ConfigurationBuilder());

                // Commons
                configuration.ForSingletonOf<IBus>().Use<Bus>();
                configuration.ForSingletonOf<IClock>().Use<Clock>();
                configuration.ForSingletonOf<IFortisConnection>().Use<FortisConnection>().Transient();
                configuration.For<IPdpExecutionRepository>().Use<PdpExecutionRepository>();
                configuration.For<IPdpParametersRepository>().Use<PdpParametersRepository>();
                configuration.ForSingletonOf<PdpParameters>();
                configuration.ForSingletonOf<AppLogger>();

                // Feeds
                configuration.For<IFeedsOrchestrator>().Use<FeedsOrchestrator>();
                configuration.For<IFeedsProcessor>().Use<FeedsProcessor>();
                configuration.For<IFeedsParserFactory>().Use<FeedsParserFactory>();
                configuration.For<IFeedsBuilder>().Use<FeedsBuilder>();
                configuration.For<IFeedRepository>().Use<FeedRepository>();
                configuration.ForSingletonOf<FeedImportReporter>();
            });
        }

        // ----- Utils
        public T GetInstance<T>()
        {
            return _container.GetInstance<T>();
        }
    }
}