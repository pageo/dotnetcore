﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;
using CommandLine;
using FluentScheduler;
using Fortis.Pdp.Common;
using Fortis.Pdp.Feeds;
using Fortis.Pdp.Utils;
using LinqToDB.Data;
using LinqToDB.DataProvider.PostgreSQL;
using Microsoft.Extensions.Configuration;

namespace Fortis.Pdp
{
    public class Program
    {
        // ----- Fields
        private static readonly FeedImportReporter FeedImportReporter = InversionOfControl.Instance.GetInstance<FeedImportReporter>();
        private static readonly PdpParameters PdpParameters = InversionOfControl.Instance.GetInstance<PdpParameters>();
        private static readonly IPdpParametersRepository PdpParametersRepository = InversionOfControl.Instance.GetInstance<IPdpParametersRepository>();
        private static readonly IFeedsOrchestrator FeedsOrchestrator = InversionOfControl.Instance.GetInstance<IFeedsOrchestrator>();
        private static readonly AppLogger Logger = InversionOfControl.Instance.GetInstance<AppLogger>();

        // ----- Main
        public static void Main(string[] args)
        {
            // Configuration
            PdpConfiguration();

            // Execution
            Execute(GetResult(args));
        }

        // ----- Internal logics
        private static void Execute(dynamic result)
        {
            try
            {
                Logger.LogInfo("START OF PDP");

                // Job
                //JobManager.Initialize(new SchedulerRegistry());

                // register
                RegisterReporters();

                // Init
                InitializePdpParameters(result);

                // Start tasks
                var task1 = ImportFeeds(result);

                // Wait end
                Task.WaitAll(task1);
            }
            catch (AggregateException ex)
            {
                Logger.LogInfo("Summary of errors that occured in the pdp :");
                foreach (var innerException in ex.InnerExceptions)
                    Logger.LogError(innerException);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
            finally
            {
                Logger.LogInfo("END OF PDP");
            }
        }

        private static void RegisterReporters()
        {
            FeedImportReporter.Register();
        }

        private static void UnRegisterReporters()
        {
            FeedImportReporter.UnRegister();
        }

        private static void InitializePdpParameters(dynamic result)
        {
            var parameters = PdpParametersRepository.GetParameters();
            parameters.CopyTo(PdpParameters);

            if (result.Force)
                PdpParameters.LastUpdateDateHandled = null;

            Logger.LogInfo("Pdp parameters has been initialized.");
        }

        private static async Task ImportFeeds(dynamic result)
        {
            var filePath = result.FeedsXmlPath;
            await FeedsOrchestrator.ProcessAsync(filePath);
        }

        private static ExpandoObject GetResult(IEnumerable<string> args)
        {
            Func<CommandLineOptions, ExpandoObject> header =
                opts =>
                {
                    dynamic parametersValues = new ExpandoObject();
                    parametersValues.Force = opts.Force;
                    parametersValues.FeedsXmlPath = opts.FeedsXmlPath;
                    return parametersValues;
                };
            var parserResult = Parser.Default.ParseArguments<CommandLineOptions>(args);
            return parserResult.MapResult(opts => header(opts), _ => null);
        }

        private static void PdpConfiguration()
        {
            var builder = InversionOfControl.Instance.GetInstance<IConfigurationBuilder>();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile("appsettings.json");

            var configuration = builder.Build();
            ConfigureDatabase(configuration);
        }

        private static void ConfigureDatabase(IConfiguration configuration)
        {
            DataConnection.AddConfiguration("Fortis", configuration.GetConnectionString("Fortis"),
                new PostgreSQLDataProvider("PostgreSQL", PostgreSQLVersion.v93));
        }
    }
}