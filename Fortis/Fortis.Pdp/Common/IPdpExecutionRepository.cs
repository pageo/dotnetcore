﻿namespace Fortis.Pdp.Common
{
    public interface IPdpExecutionRepository
    {
        PdpExecution GetLast(string type);
        void Create(PdpExecution newPdpExecution);
    }
}
