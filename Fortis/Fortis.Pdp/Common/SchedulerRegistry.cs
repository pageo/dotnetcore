﻿using FluentScheduler;

namespace Fortis.Pdp.Common
{
    public class SchedulerRegistry : Registry
    {
        public SchedulerRegistry()
        {
            Schedule<PdpJob>().ToRunNow().AndEvery(2).Minutes();
        }
    }
}
