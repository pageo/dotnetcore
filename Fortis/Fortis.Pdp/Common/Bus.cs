﻿using System;
using System.Collections.Generic;

namespace Fortis.Pdp.Common
{
    public class Bus : IBus
    {
        // ----- Fields
        public static Bus Instance { get; } = new Bus();

        private readonly Dictionary<Type, List<Delegate>> _callbacks = new Dictionary<Type, List<Delegate>>();
        private readonly Dictionary<object, List<Delegate>> _ownedCallbacks = new Dictionary<object, List<Delegate>>();

        // ----- Public methods
        public void Send<T>(T message)
        {
            if (_callbacks.ContainsKey(typeof(T))) {
                foreach (var callback in _callbacks[typeof(T)].ToArray()) {
                    callback.DynamicInvoke(message);
                }
            }
        }

        public void Register<T>(object subscriber, Action<T> callback)
        {
            RegisterDelegate<T>(subscriber, callback);
        }

        public void Unregister(object subscriber)
        {
            if (_ownedCallbacks.ContainsKey(subscriber)) {
                foreach (var @delegate in _ownedCallbacks[subscriber]) {
                    var messageType = @delegate.GetType().GenericTypeArguments[0];
                    if (_callbacks.ContainsKey(messageType))
                        _callbacks[messageType].Remove(@delegate);
                }
                _ownedCallbacks.Remove(subscriber);
            }
        }

        // ----- Utils
        private void RegisterDelegate<T>(object subscriber, Delegate callback)
        {
            if (_callbacks.ContainsKey(typeof(T)) == false)
                _callbacks[typeof(T)] = new List<Delegate>();
            _callbacks[typeof(T)].Add(callback);

            if (_ownedCallbacks.ContainsKey(subscriber) == false)
                _ownedCallbacks[subscriber] = new List<Delegate>();
            _ownedCallbacks[subscriber].Add(callback);
        }
    }
}
