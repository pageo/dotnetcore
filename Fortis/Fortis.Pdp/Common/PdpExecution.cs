﻿using System;
using LinqToDB.Mapping;

namespace Fortis.Pdp.Common
{
    [Table(Schema = "fortis", Name = "PdpExecution")]
    public class PdpExecution
    {
        [Column, Identity]
        public int Id { get; set; }

        [Column]
        public string Type { get; set; }

        [Column]
        public DateTime? LastUpdateDateHandled { get; set; }

        [Column]
        public DateTime StartDate { get; set; }

        [Column]
        public DateTime EndDate { get; set; }

        [Column]
        public int InsertedCount { get; set; }

        [Column]
        public int DeletedCount { get; set; }

        [Column]
        public int RejectedCount { get; set; }
    }
}
