﻿using FluentScheduler;
using Fortis.Pdp.Utils;

namespace Fortis.Pdp.Common
{
    public class PdpJob : IJob
    {
        // ----- Fields
        private readonly AppLogger _logger;
        private readonly object _lock = new object();

        // ----- Constructors
        public PdpJob(AppLogger logger)
        {
            _logger = logger;
        }

        // ----- Constructors
        public void Execute()
        {
            lock (_lock) {
                // Do work, son!
                _logger.LogInfo("Hello world !");
            }
        }
    }
}
