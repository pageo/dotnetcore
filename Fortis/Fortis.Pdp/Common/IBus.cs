﻿using System;

namespace Fortis.Pdp.Common
{
    public interface IBus
    {
        void Send<T>(T message);
        void Register<T>(object subscriber, Action<T> callbacks);
        void Unregister(object subscriber);
    }
}