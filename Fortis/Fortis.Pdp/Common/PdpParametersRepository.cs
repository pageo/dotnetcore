﻿using Microsoft.Extensions.Configuration;

namespace Fortis.Pdp.Common
{
    public class PdpParametersRepository : IPdpParametersRepository
    {
        // ----- Fields
        private readonly IPdpExecutionRepository _pdpExecutionRepository;
        private readonly IConfigurationBuilder _configurationBuilder;

        // ----- Constructors
        public PdpParametersRepository(IPdpExecutionRepository pdpExecutionRepository, IConfigurationBuilder configurationBuilder)
        {
            _pdpExecutionRepository = pdpExecutionRepository;
            _configurationBuilder = configurationBuilder;
        }

        // ----- Public methods
        public PdpParameters GetParameters()
        {
            var lastPdpExecution = _pdpExecutionRepository.GetLast("FEEDS");
            var configuration = _configurationBuilder.Build();
            return new PdpParameters
            {
                BeforeNowMonthCount = int.Parse(configuration.GetSection("Settings:BeforeNowMonthCount").Value),
                AfterNowMonthCount = int.Parse(configuration.GetSection("Settings:AfterNowMonthCount").Value),
                LastUpdateDateHandled = lastPdpExecution?.LastUpdateDateHandled
            };
        }
    }
}
