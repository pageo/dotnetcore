﻿namespace Fortis.Pdp.Common
{
    public interface IPdpParametersRepository
    {
        PdpParameters GetParameters();
    }
}
