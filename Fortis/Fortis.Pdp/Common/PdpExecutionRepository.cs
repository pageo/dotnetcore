﻿using System.Linq;
using Fortis.Pdp.Database;

namespace Fortis.Pdp.Common
{
    public class PdpExecutionRepository : IPdpExecutionRepository
    {
        private readonly IFortisConnection _connection;

        public PdpExecutionRepository(IFortisConnection connection)
        {
            _connection = connection;
        }

        public PdpExecution GetLast(string type)
        {
            return _connection.PdpExecutions
                .Where(x => x.Type == type)
                .OrderByDescending(x => x.StartDate)
                .FirstOrDefault();
        }

        public void Create(PdpExecution newPdpExecution)
        {
            _connection.Insert(newPdpExecution);
        }
    }
}
