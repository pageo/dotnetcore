﻿using System;

namespace Fortis.Pdp.Common
{
    public class PdpParameters
    {
        public int BeforeNowMonthCount { get; set; } = 6;
        public int AfterNowMonthCount { get; set; } = 12;
        public DateTime? LastUpdateDateHandled { get; set; }
    }
}
