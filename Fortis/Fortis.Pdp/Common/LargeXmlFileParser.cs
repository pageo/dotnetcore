﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Fortis.Pdp.Common
{
    public abstract class LargeXmlFileParser<T> where T : class
    {
        // ----- Fields
        private readonly XmlSerializer _serializer = new XmlSerializer(typeof(T));
        private readonly XmlReader _reader;

        // ----- Properties
        protected abstract string XmlTag { get; }
        public bool Ended => _reader.EOF;

        // ----- Constructors
        protected LargeXmlFileParser(string xmlPath)
        {
            _reader = XmlReader.Create(xmlPath);
        }

        // ----- Public methods
        public T ParseNextElement()
        {
            if (_reader.EOF)
                return null;

            if (_reader.NodeType != XmlNodeType.Element || _reader.Name != XmlTag)
            {
                var readOk = _reader.ReadToFollowing(XmlTag);
                if (!readOk)
                    return null;
            }

            var xml = _reader.ReadOuterXml();
            return (T)_serializer.Deserialize(new StringReader(xml));
        }

        public void Dispose()
        {
            _reader.Dispose();
        }
    }
}
