﻿using System;

namespace Fortis.Pdp.Utils
{
    public class Period
    {
        public DateTime Start { get; }
        public DateTime End { get; }
        public TimeSpan Duration => End.Subtract(Start);

        public Period(DateTime start, DateTime end)
        {
            if (start > end)
            {
                throw new ArgumentException("The start date should be before the end date to create a valid period.");
            }

            Start = start;
            End = end;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Period))
            {
                return base.Equals(obj);
            }
            return Equals((Period)obj);
        }
        protected bool Equals(Period other)
        {
            return Start.Equals(other.Start) && End.Equals(other.End);
        }
        public override int GetHashCode()
        {
            unchecked
            {
                return (Start.GetHashCode() * 397) ^ End.GetHashCode();
            }
        }

        public bool Contains(Period period)
        {
            return Contains(period.Start) && Contains(period.End);
        }

        public bool Contains(DateTime date)
        {
            return date >= Start && date <= End;
        }

        public override string ToString()
        {
            return Start + " -> " + End;
        }
        public bool Intersect(Period period)
        {
            return Contains(period.Start) || Contains(period.End);
        }
    }
}
