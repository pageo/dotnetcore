﻿using System;
using System.Linq;
using System.Reflection;

namespace Fortis.Pdp.Utils
{
    public static class ObjectExtensions
    {
        public static T Clone<T>(this T source)
        {
            var clone = (T)Activator.CreateInstance(typeof(T));
            source.CopyTo(clone);
            return clone;
        }

        public static void CopyTo<T>(this T source, T target)
        {
            var readableAndWriteableProperties = typeof(T)
                .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(x => x.CanRead && x.CanWrite);

            foreach (var propertyInfo in readableAndWriteableProperties)
            {
                propertyInfo.SetValue(target, propertyInfo.GetValue(source));
            }
        }
    }
}
