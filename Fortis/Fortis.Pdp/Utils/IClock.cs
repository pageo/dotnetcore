﻿using System;

namespace Fortis.Pdp.Utils
{
    public interface IClock
    {
        DateTime Now();
    }
}
