﻿using NLog;

namespace Fortis.Pdp.Utils
{
    public class AppLogger
    {
        // ----- Fields
        private readonly ILogger _nLogger;

        // ----- Constructors
        public AppLogger()
        {
            _nLogger = LogManager.GetCurrentClassLogger();
        }

        // ----- Public methods
        public void LogError(object obj)
        {
            try
            {
                _nLogger.Error(obj);
            }
            catch
            {
                // ignored
            }
        }

        public void LogWarning(object obj)
        {
            try
            {
                _nLogger.Warn(obj);
            }
            catch
            {
                // ignored
            }
        }

        public void LogInfo(object obj)
        {
            try
            {
                _nLogger.Info(obj);
            }
            catch
            {
                // ignored
            }
        }

        public void LogDebug(object obj)
        {
            try
            {
                _nLogger.Debug(obj);
            }
            catch
            {
                // ignored
            }
        }
    }
}