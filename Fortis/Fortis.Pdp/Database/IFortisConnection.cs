﻿using System.Collections.Generic;
using System.Linq;
using Fortis.Pdp.Common;
using Fortis.Pdp.Feeds;
using LinqToDB.Data;

namespace Fortis.Pdp.Database
{
    public interface IFortisConnection
    {
        void BeginTransaction();
        void CommitTransaction();
        void RollbackTransaction();

        int Execute(string sql);
        DataReader ExecuteReader(string sql);

        IQueryable<PdpExecution> PdpExecutions { get; }
        IQueryable<Feed> Feeds { get; }

        void Insert<T>(T element);
        void MassiveInsert<T>(IEnumerable<T> elements);
    }
}
