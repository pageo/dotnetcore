﻿using System.Collections.Generic;
using System.Linq;
using Fortis.Pdp.Common;
using Fortis.Pdp.Feeds;
using LinqToDB;
using LinqToDB.Data;

namespace Fortis.Pdp.Database
{
    public class FortisConnection : DataConnection, IFortisConnection
    {
        // ----- Constructors
        public FortisConnection() : base("Fortis")
        {
        }

        // ----- Public methods
        public IQueryable<PdpExecution> PdpExecutions => GetTable<PdpExecution>();
        public IQueryable<Feed> Feeds => GetTable<Feed>();


        // ----- Adapting interface
        void IFortisConnection.BeginTransaction()
        {
            BeginTransaction();
        }
        int IFortisConnection.Execute(string sql)
        {
            return this.Execute(sql);
        }

        DataReader IFortisConnection.ExecuteReader(string sql)
        {
            return this.ExecuteReader(sql);
        }

        void IFortisConnection.Insert<T>(T element)
        {
            this.InsertWithIdentity(element);
        }
        void IFortisConnection.MassiveInsert<T>(IEnumerable<T> elements)
        {
            this.BulkCopy(elements);
        }
    }
}
