﻿using System;

namespace Fortis.Pdp.Models
{
    public class Info
    {
        public string Name { get; set; }
        public string ProfilePic { get; set; }
        public int Hits { get; set; }
        public string Image { get; set; }
        public string Status { get; set; }
        public string Url { get; set; }
        public DateTime? ModificationDate { get; set; }
    }
}