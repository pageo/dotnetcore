﻿using CommandLine;

namespace Fortis.Pdp
{
    public class CommandLineOptions
    {
        [Option("feeds", Required = true, HelpText = "Défini le chemin vers le fichier Feeds.xml à traiter.")]
        public string FeedsXmlPath { get; set; }

        [Option('f', "force", Default = false, HelpText = "Défini si l'on ignore la date de dernière modification prise en compte, et que l'on force la mise à jour de toute la base de données.")]
        public bool Force { get; set; }
    }
}
