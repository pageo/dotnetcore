﻿using System;

namespace Fortis.Pdp.Feeds.Events
{
    public class FeedsImportStarted
    {
        public DateTime StartDate { get; } = DateTime.Now;
    }
}
