﻿using System;

namespace Fortis.Pdp.Feeds.Events
{
    public class FeedsImportEnded
    {
        public DateTime EndDate { get; } = DateTime.Now;
    }
}
