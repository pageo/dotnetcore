﻿using System;

namespace Fortis.Pdp.Feeds.Events
{
    public class FeedsImportFailed
    {
        public Exception Exception { get; }

        public FeedsImportFailed(Exception exception)
        {
            Exception = exception;
        }
    }
}
