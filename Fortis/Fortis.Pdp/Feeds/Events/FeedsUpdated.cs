﻿namespace Fortis.Pdp.Feeds.Events
{
    public class FeedsUpdated
    {
        public int InsertedCount { get; }
        public int DeletedCount { get; }

        public FeedsUpdated(int insertedCount, int deletedCount)
        {
            InsertedCount = insertedCount;
            DeletedCount = deletedCount;
        }
    }
}
