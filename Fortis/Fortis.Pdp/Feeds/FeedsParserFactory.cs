﻿namespace Fortis.Pdp.Feeds
{
    public class FeedsParserFactory : IFeedsParserFactory
    {
        // ----- Public methods
        public IFeedsParser Open(string feedsFilePath)
        {
            return new FeedsParser(feedsFilePath);
        }
    }
}