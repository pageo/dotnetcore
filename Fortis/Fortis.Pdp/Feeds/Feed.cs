﻿using System;
using LinqToDB.Mapping;

namespace Fortis.Pdp.Feeds
{
    [Table(Schema = "fortis", Name = "Feed")]
    public class Feed
    {
        [PrimaryKey, Identity]
        public int Id { get; set; }

        [Column]
        public string Name { get; set; }

        [Column]
        public string Image { get; set; }

        [Column]
        public string Status { get; set; }

        [Column]
        public string ProfilePic { get; set; }

        [Column]
        public string Url { get; set; }

        [Column]
        public int Hits { get; set; }

        [Column, NotNull]
        public DateTime? ModificationDate { get; set; }
    }
}
