﻿using System.Collections.Generic;
using Fortis.Pdp.Models;

namespace Fortis.Pdp.Feeds
{
    public interface IFeedsBuilder
    {
        IReadOnlyList<Feed> Build(Data data);
    }
}