﻿using System.Threading.Tasks;

namespace Fortis.Pdp.Feeds
{
    public interface IFeedsOrchestrator
    {
        void Process(string feedsFilePath);
        Task ProcessAsync(string feedsXmlPath);
    }
}
