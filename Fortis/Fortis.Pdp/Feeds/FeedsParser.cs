﻿using Fortis.Pdp.Common;
using Fortis.Pdp.Models;

namespace Fortis.Pdp.Feeds
{
    public class FeedsParser : LargeXmlFileParser<Data>, IFeedsParser
    {
        // ----- Fields
        protected override string XmlTag => "Data";

        // ----- Constructors
        public FeedsParser(string xmlPath) : base(xmlPath)
        {
        }

        // ----- Public methods
        public Data ParseNextData()
        {
            return ParseNextElement();
        }
    }
}