﻿using System.Collections.Generic;
using System.Linq;
using Fortis.Pdp.Common;
using Fortis.Pdp.Models;
using Fortis.Pdp.Utils;

namespace Fortis.Pdp.Feeds
{
    public class FeedsBuilder : IFeedsBuilder
    {
        // ----- Fields
        private readonly PdpParameters _parameters;
        private readonly AppLogger _logger;

        // ----- Constructor
        public FeedsBuilder(
            PdpParameters parameters,
            AppLogger logger)
        {
            _parameters = parameters;
            _logger = logger;
        }

        // ----- Public methods
        public IReadOnlyList<Feed> Build(Data data)
        {
            return GetFeedsFromInfos(data.Infos.ToArray()).ToArray();
        }

        // ----- Internal logics
        private IEnumerable<Feed> GetFeedsFromInfos(IReadOnlyCollection<Info> infos)
        {
            _logger.LogInfo($"{infos?.Count} infos trouvés.");
            return infos
                .Select(x => new Feed
                {
                    Name = x.Name,
                    Image = x.Image,
                    Hits = x.Hits,
                    Status = x.Status,
                    ProfilePic = x.ProfilePic,
                    Url = x.Url,
                    ModificationDate = _parameters.LastUpdateDateHandled ?? x.ModificationDate
                }).ToArray();
        }
    }
}