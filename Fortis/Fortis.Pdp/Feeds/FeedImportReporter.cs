﻿using Fortis.Pdp.Common;
using Fortis.Pdp.Feeds.Events;
using Fortis.Pdp.Utils;

namespace Fortis.Pdp.Feeds
{
    public class FeedImportReporter
    {
        // ----- Fields
        private readonly IBus _bus;
        private readonly IPdpExecutionRepository _pdpExecutionRepository;
        private readonly PdpParameters _parameters;
        private readonly AppLogger _logger;
        private FeedsImportStarted _started;
        private int _totalInsertedCount;
        private int _totalDeletedCount;

        // ----- Constructors
        public FeedImportReporter(
            IBus bus,
            IPdpExecutionRepository pdpExecutionRepository,
            PdpParameters parameters,
            AppLogger logger)
        {
            _bus = bus;
            _pdpExecutionRepository = pdpExecutionRepository;
            _parameters = parameters;
            _logger = logger;
        }

        // ----- Public methods
        public void Register()
        {
            _bus.Register<FeedsImportStarted>(this, OnFeedsImportStarted);
            _bus.Register<FeedsUpdated>(this, OnFeedsUpdated);
            _bus.Register<FeedsImportEnded>(this, OnFeedsImportEnded);
            _bus.Register<FeedsRefDataFileLoaded>(this, OnFeedsRefDataFileLoaded);
            _bus.Register<FeedsImportFailed>(this, OnFeedsImportFailed);
        }

        public void UnRegister()
        {
            _bus.Unregister(this);
        }

        // ----- Callbacks
        private void OnFeedsImportStarted(FeedsImportStarted @event)
        {
            _started = @event;
            _logger.LogInfo("Start of the feeds import.");
        }

        private void OnFeedsUpdated(FeedsUpdated @event)
        {
            _totalInsertedCount += @event.InsertedCount;
            _totalDeletedCount += @event.DeletedCount;
            _logger.LogDebug(Format("FEEDS", @event.InsertedCount, @event.DeletedCount, 0));
        }

        private void OnFeedsImportEnded(FeedsImportEnded @event)
        {
            var period = new Period(_started.StartDate, @event.EndDate);

            _logger.LogInfo($"End of the assets import ({period.Duration:hh\\:mm\\:ss}) : {_totalInsertedCount} inserted, {_totalDeletedCount} deleted, lastModificationDate={_parameters.LastUpdateDateHandled}");

            _pdpExecutionRepository.Create(new PdpExecution
            {
                Type = "FEEDS",
                StartDate = _started.StartDate,
                EndDate = @event.EndDate,
                LastUpdateDateHandled = _parameters.LastUpdateDateHandled,
                InsertedCount = _totalInsertedCount,
                DeletedCount = _totalDeletedCount,
                RejectedCount = 0
            });
        }

        private void OnFeedsRefDataFileLoaded(FeedsRefDataFileLoaded @event)
        {
            _logger.LogInfo("FeedsRefData.xml loaded");
        }

        private void OnFeedsImportFailed(FeedsImportFailed @event)
        {
            _logger.LogError($"An error occured during the feeds import {@event.Exception}");
        }

        // ----- Utils
        private static string Format(string type, int insertedCount, int deletedCount, int rejectedCount)
        {
            return
                $"{type.PadRight(12)} : inserted={insertedCount.ToString().PadRight(8)}\t deleted={deletedCount.ToString().PadRight(8)}\t rejected={rejectedCount.ToString().PadRight(8)}";
        }
    }
}