﻿namespace Fortis.Pdp.Feeds
{
    public interface IFeedsParserFactory
    {
        IFeedsParser Open(string feedsFilePath);
    }
}