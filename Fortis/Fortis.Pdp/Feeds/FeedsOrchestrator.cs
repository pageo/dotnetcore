﻿using System;
using System.Threading.Tasks;
using Fortis.Pdp.Common;
using Fortis.Pdp.Feeds.Events;
using Fortis.Pdp.Models;
using Fortis.Pdp.Utils;

namespace Fortis.Pdp.Feeds
{
    public class FeedsOrchestrator : IFeedsOrchestrator
    {
        // ----- Fields
        private readonly IBus _bus;
        private readonly IFeedsParserFactory _parserFactory;
        private readonly IFeedsProcessor _processor;
        private readonly AppLogger _logger;

        // ----- Constructors
        public FeedsOrchestrator(IBus bus,
            IFeedsParserFactory parserFactory,
            IFeedsProcessor processor,
            AppLogger logger)
        {
            _bus = bus;
            _parserFactory = parserFactory;
            _processor = processor;
            _logger = logger;
        }

        // ----- Public methods
        public Task ProcessAsync(string feedsXmlPath)
        {
            return Task.Factory.StartNew(() => Process(feedsXmlPath));
        }

        public void Process(string feedsFilePath)
        {
            try
            {
                ProcessFeedsFile(feedsFilePath);
            }
            catch (Exception ex)
            {
                _bus.Send(new FeedsImportFailed(ex));
                throw;
            }
        }

        // ----- Internal logic
        private void ProcessFeedsFile(string feedsFilePath)
        {
            _bus.Send(new FeedsImportStarted());
            using (var parser = _parserFactory.Open(feedsFilePath))
            {
                while (!parser.Ended)
                {
                    var data = SafeParse(parser);
                    if (data != null)
                        _processor.Process(data);
                }
                _processor.EndProcess();
            }
            _bus.Send(new FeedsImportEnded());
        }

        private Data SafeParse(IFeedsParser parser)
        {
            try
            {
                return parser.ParseNextData();
            }
            catch (Exception ex)
            {
                _logger.LogError("Error when parsing data from xml. Probably an xsd error.");
                _logger.LogError(ex);
                return null;
            }
        }
    }
}