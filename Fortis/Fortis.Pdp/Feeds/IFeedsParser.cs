﻿using System;
using Fortis.Pdp.Models;

namespace Fortis.Pdp.Feeds
{
    public interface IFeedsParser : IDisposable
    {
        Data ParseNextData();
        bool Ended { get; }
    }
}