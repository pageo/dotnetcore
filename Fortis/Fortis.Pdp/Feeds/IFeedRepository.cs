﻿using System.Collections.Generic;

namespace Fortis.Pdp.Feeds
{
    public interface IFeedRepository
    {
        int DeleteFeeds(string[] names);
        void InsertFeeds(IEnumerable<Feed> feeds);
    }
}
