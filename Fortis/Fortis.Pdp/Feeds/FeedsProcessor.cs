﻿using System.Collections.Generic;
using System.Linq;
using Fortis.Pdp.Common;
using Fortis.Pdp.Database;
using Fortis.Pdp.Feeds.Events;
using Fortis.Pdp.Models;
using Fortis.Pdp.Utils;
using Npgsql;

namespace Fortis.Pdp.Feeds
{
    public class FeedsProcessor : IFeedsProcessor
    {
        // ----- Fields
        private readonly IBus _bus;
        private readonly IFeedsBuilder _builder;
        private readonly IFeedRepository _repository;
        private readonly IFortisConnection _connection;
        private readonly IClock _clock;
        private readonly PdpParameters _parameters;
        private readonly List<Feed> _feedsToProcess = new List<Feed>();
        
        // ----- Properties
        public int TransactionSize { get; set; } = 250;

        // ----- Constructors
        public FeedsProcessor(
            IBus bus,
            IFeedsBuilder builder,
            IFeedRepository repository,
            IFortisConnection connection,
            IClock clock,
            PdpParameters parameters)
        {
            _bus = bus;
            _builder = builder;
            _repository = repository;
            _connection = connection;
            _clock = clock;
            _parameters = parameters;
        }

        // ----- Public methods
        public void Process(Data data)
        {
            var feeds = _builder.Build(data);

            if (feeds.Any())
                AddFeedsToProcess(feeds);
        }

        public void EndProcess()
        {
            if (_feedsToProcess.Any())
                ProcessCurrentFeedsInASingleTransaction();
        }

        // ----- Internal logic
        private void AddFeedsToProcess(IReadOnlyList<Feed> feeds)
        {
            _feedsToProcess.AddRange(feeds);
            if (_feedsToProcess.Count >= TransactionSize) 
                ProcessCurrentFeedsInASingleTransaction();
        }

        private void ProcessCurrentFeedsInASingleTransaction()
        {
            var feedsNames = GetNameFromFeeds(_feedsToProcess);
            try
            {
                _connection.BeginTransaction();
                var deletedAssetCount = _repository.DeleteFeeds(feedsNames);
                _repository.InsertFeeds(_feedsToProcess);
                _connection.CommitTransaction();

                _bus.Send(new FeedsUpdated(_feedsToProcess.Count, deletedAssetCount));
                _feedsToProcess.Clear();
            }
            catch (PostgresException)
            {
                _connection.RollbackTransaction();
                throw;
            }
        }

        private static string[] GetNameFromFeeds(IEnumerable<Feed> feeds)
        {
            return feeds
                .Where(feed => !string.IsNullOrEmpty(feed.Name))
                .Select(feed => feed.Name)
                .Distinct()
                .ToArray();
        }
    }
}