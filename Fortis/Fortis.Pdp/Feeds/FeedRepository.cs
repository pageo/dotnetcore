﻿using System.Collections.Generic;
using System.Linq;
using Fortis.Pdp.Database;
using LinqToDB;

namespace Fortis.Pdp.Feeds
{
    public class FeedRepository : IFeedRepository
    {
        private readonly IFortisConnection _connection;

        public FeedRepository(IFortisConnection connection)
        {
            _connection = connection;
        }

        public int DeleteFeeds(string[] names)
        {
           return _connection.Feeds
                .Where(x => names.Contains(x.Name))
                .Delete();
        }

        public void InsertFeeds(IEnumerable<Feed> feeds)
        {
            _connection.MassiveInsert(feeds);
        }
    }
}
