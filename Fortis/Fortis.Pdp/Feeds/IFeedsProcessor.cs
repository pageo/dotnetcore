﻿using Fortis.Pdp.Models;

namespace Fortis.Pdp.Feeds
{
    public interface IFeedsProcessor
    {
        void Process(Data data);
        void EndProcess();
    }
}