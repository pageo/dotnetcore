﻿namespace Fortis.Domain.Validators
{
    public interface IValidator<T>
    {
        bool Validate(T model);
    }
}