﻿using System.Collections.Generic;

namespace Fortis.Domain.Feeds
{
    public class GetFeedsCommandHandler: IHandler<GetFeedsCommand, IEnumerable<Feed>>
    {
        // ----- Fields
        private readonly IFeedRepository _feedRepository;

        // ----- Constructors
        public GetFeedsCommandHandler(
            IFeedRepository feedRepository)
        {
            _feedRepository = feedRepository;
        }

        // ----- Public methods
        public IEnumerable<Feed> Handle(GetFeedsCommand command)
        {
            return _feedRepository.GetAllFeeds();
        }
    }
}