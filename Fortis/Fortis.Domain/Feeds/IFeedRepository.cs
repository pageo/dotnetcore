﻿using System.Collections.Generic;

namespace Fortis.Domain.Feeds
{
    public interface IFeedRepository
    {
        IEnumerable<Feed> GetAllFeeds();
        Feed GetFeedById(int id);
    }
}