﻿namespace Fortis.Domain.Feeds
{
    public class Feed
    {
        // ----- Properties
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public string Status { get; set; }
        public string ProfilePic { get; set; }
        public string Url { get; set; }
        public int Hits { get; set; }

        // ----- Override methods
        public override string ToString() => $"Id: {Id}, Name: {Name}, Image: {Image}, Status: {Status}, ProfilePic: {ProfilePic}, Url: {Url}";
    }
}