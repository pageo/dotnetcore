﻿namespace Fortis.Domain.Feeds
{
    public class GetFeedsCommand : IAuthenticable
    {
        // ----- Properties
        public int UserId { get; }

        // ----- Constructors
        public GetFeedsCommand(int userId)
        {
            UserId = userId;
        }
    }
}