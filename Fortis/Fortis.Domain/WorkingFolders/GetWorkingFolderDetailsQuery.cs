﻿namespace Fortis.Domain.WorkingFolders
{
    public class GetWorkingFolderDetailsQuery : IAuthenticable
    {
        // ----- Properties
        public int UserId { get; }
        public int WorkingFolderId { get; }

        // ----- Constructors
        public GetWorkingFolderDetailsQuery(int userId, int workingFolderId)
        {
            UserId = userId;
            WorkingFolderId = workingFolderId;
        }
    }
}