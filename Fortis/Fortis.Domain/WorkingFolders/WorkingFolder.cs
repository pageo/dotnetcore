﻿using System;

namespace Fortis.Domain.WorkingFolders
{
    public class WorkingFolder
    {
        // ----- Properties
        public int Id { get; }
        public string Name { get; }
        public int Owner { get; }

        // ----- Constructors
        public WorkingFolder(string name, int owner)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Owner = owner;
        }
        public WorkingFolder(int id, string name, int owner) : this(name, owner)
        {
            Id = id;
        }
    }
}
