﻿using Fortis.Domain.WorkingFolders.Exceptions;

namespace Fortis.Domain.WorkingFolders
{
    public class DeleteWorkingFolderCommandHandler : IHandler<DeleteWorkingFolderCommand>
    {
        // ----- Fields
        private readonly IWorkingFolderRepository _workingFolderRepository;

        // ----- Constructors
        public DeleteWorkingFolderCommandHandler(IWorkingFolderRepository workingFolderRepository)
        {
            _workingFolderRepository = workingFolderRepository;
        }

        // ----- Public methods
        public void Handle(DeleteWorkingFolderCommand command)
        {
            var workingFolder = _workingFolderRepository.Get(command.WorkingFolderId);
            if (workingFolder == null)
                throw new WorkingFolderNotFound(command.WorkingFolderId);

            if (workingFolder.Owner == command.UserId)
                _workingFolderRepository.Delete(workingFolder.Id);
        }
    }
}
