﻿using System;

namespace Fortis.Domain.WorkingFolders
{
    public class WorkingFolderListItem
    {
        // ----- Properties
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsOwner { get; set; }
        public DateTime CreationDate { get; set; }
    }
}