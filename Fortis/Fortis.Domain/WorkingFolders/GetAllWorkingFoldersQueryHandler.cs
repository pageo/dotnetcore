﻿using System.Collections.Generic;

namespace Fortis.Domain.WorkingFolders
{
    public class GetAllWorkingFoldersQueryHandler : IHandler<GetAllWorkingFoldersQuery, IEnumerable<WorkingFolderListItem>>
    {
        // ----- Fields
        private readonly IWorkingFolderRepository _workingFolderRepository;

        // ----- Constructors
        public GetAllWorkingFoldersQueryHandler(IWorkingFolderRepository workingFolderRepository)
        {
            _workingFolderRepository = workingFolderRepository;
        }

        // ----- Public methods
        public IEnumerable<WorkingFolderListItem> Handle(GetAllWorkingFoldersQuery command)
        {
            return _workingFolderRepository.GetByUserId(command.UserId);
        }
    }
}
