﻿using Fortis.Domain.WorkingFolders.Exceptions;

namespace Fortis.Domain.WorkingFolders
{
    public class SaveWorkingFolderCommandHandler : IHandler<SaveWorkingFolderCommand>
    {
        // ----- Fields
        private readonly IWorkingFolderRepository _workingFolderRepository;

        // ----- Constructors
        public SaveWorkingFolderCommandHandler(IWorkingFolderRepository workingFolderRepository)
        {
            _workingFolderRepository = workingFolderRepository;
        }

        // ----- Public methods
        public void Handle(SaveWorkingFolderCommand command)
        {
            var workingFolder = _workingFolderRepository.Get(command.WorkingFolderId);
            if (workingFolder == null)
                throw new WorkingFolderNotFound(command.WorkingFolderId);

            _workingFolderRepository.Save(command.UserId, workingFolder);
        }
    }
}
