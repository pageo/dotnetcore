﻿using System;

namespace Fortis.Domain.WorkingFolders.Exceptions
{
    public class WorkingFolderNotFound : Exception
    {
        // ----- Constructors
        public WorkingFolderNotFound(int workingFolderId) : base(BuildMessage(workingFolderId))
        {
        }

        // ----- Internal logics
        private static string BuildMessage(int workingFolderId)
        {
            return $"The working folder with id '{workingFolderId}' was not found.";
        }
    }
}