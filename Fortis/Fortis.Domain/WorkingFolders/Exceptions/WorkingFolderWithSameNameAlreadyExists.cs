﻿using System;

namespace Fortis.Domain.WorkingFolders.Exceptions
{
    public class WorkingFolderWithSameNameAlreadyExists : Exception
    {
        // ----- Constructors
        public WorkingFolderWithSameNameAlreadyExists(string message) : base(message)
        {
        }
    }
}