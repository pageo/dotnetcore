﻿using System;

namespace Fortis.Domain.WorkingFolders.Exceptions
{
    public class InvalidWorkingFolderName : Exception
    {
        // ----- Constructors
        public InvalidWorkingFolderName(string message) : base(message)
        {
        }
    }
}