﻿namespace Fortis.Domain.WorkingFolders
{
    public class DeleteWorkingFolderCommand : IAuthenticable
    {
        // ----- Properties
        public int UserId { get; }
        public int WorkingFolderId { get; }

        // ----- Constructors
        public DeleteWorkingFolderCommand(int userId, int workingFolderId)
        {
            UserId = userId;
            WorkingFolderId = workingFolderId;
        }
    }
}