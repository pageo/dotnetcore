﻿namespace Fortis.Domain.WorkingFolders
{
    public class GetAllWorkingFoldersQuery : IAuthenticable
    {
        // ----- Properties
        public int UserId { get; }

        // ----- Constructors
        public GetAllWorkingFoldersQuery(int userId)
        {
            UserId = userId;
        }
    }
}