﻿namespace Fortis.Domain.WorkingFolders
{
    public class CreateNewWorkingFolderCommand : IAuthenticable
    {
        // ----- Properties
        public string Name { get; }
        public int UserId { get; }

        // ----- Constructors
        public CreateNewWorkingFolderCommand(string name, int userId)
        {
            Name = name;
            UserId = userId;
        }
    }
}