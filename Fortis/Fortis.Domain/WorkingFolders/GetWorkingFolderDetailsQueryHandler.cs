﻿namespace Fortis.Domain.WorkingFolders
{
    public class GetWorkingFolderDetailsQueryHandler : IHandler<GetWorkingFolderDetailsQuery, WorkingFolderDetails>
    {
        // ----- Fields
        private readonly IWorkingFolderRepository _workingFolderRepository;

        // ----- Constructors
        public GetWorkingFolderDetailsQueryHandler(IWorkingFolderRepository workingFolderRepository)
        {
            _workingFolderRepository = workingFolderRepository;
        }

        // ----- Public methods
        public WorkingFolderDetails Handle(GetWorkingFolderDetailsQuery detailsQuery)
        {
            return _workingFolderRepository.GetReadModel(detailsQuery.WorkingFolderId);
        }
    }
}
