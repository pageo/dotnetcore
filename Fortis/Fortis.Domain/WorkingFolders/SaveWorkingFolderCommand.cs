﻿namespace Fortis.Domain.WorkingFolders
{
    public class SaveWorkingFolderCommand : IAuthenticable
    {
        // ----- Properties
        public int UserId { get; }
        public int WorkingFolderId { get; }

        // ---- Constructors
        public SaveWorkingFolderCommand(int userId, int workingFolderId)
        {
            UserId = userId;
            WorkingFolderId = workingFolderId;
        }
    }
}