﻿using System.ComponentModel.DataAnnotations;

namespace Fortis.Domain.WorkingFolders
{
    public class CreateWorkingFolderParameters
    {
        [Required]
        public string Name { get; set; }
    }
}