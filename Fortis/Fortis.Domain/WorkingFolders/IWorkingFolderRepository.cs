﻿using System.Collections.Generic;

namespace Fortis.Domain.WorkingFolders
{
    public interface IWorkingFolderRepository
    {
        IEnumerable<WorkingFolderListItem> GetByUserId(int userId);
        WorkingFolder Get(int workingFolderId);
        long Create(WorkingFolder workingFolder);
        bool Exists(string workingFolderName, long userId);
        bool Exists(int workingFolderId);
        WorkingFolderDetails GetReadModel(int workingFolderId);
        void Save(int userId, WorkingFolder workingFolder);
        void Delete(int workingFolderId);
    }
}
