﻿using Fortis.Domain.WorkingFolders.Exceptions;

namespace Fortis.Domain.WorkingFolders
{
    public class CreateNewWorkingFolderCommandHandler : IHandler<CreateNewWorkingFolderCommand, long>
    {
        // ----- Fields
        private readonly IWorkingFolderRepository _workingFolderRepository;

        // ----- Constructors
        public CreateNewWorkingFolderCommandHandler(
            IWorkingFolderRepository workingFolderRepository)
        {
            _workingFolderRepository = workingFolderRepository;
        }

        // ----- Public methods
        public long Handle(CreateNewWorkingFolderCommand command)
        {
            if (string.IsNullOrEmpty(command.Name) || string.IsNullOrWhiteSpace(command.Name))
                throw new InvalidWorkingFolderName("A working folder should have a non empty name.");

            var exists = _workingFolderRepository.Exists(command.Name, command.UserId);
            if (exists)
                throw new WorkingFolderWithSameNameAlreadyExists($"A working folder with the name {command.Name} already exist.");

            var workingFolder = new WorkingFolder(command.Name, command.UserId);
            return _workingFolderRepository.Create(workingFolder);
        }
    }
}
