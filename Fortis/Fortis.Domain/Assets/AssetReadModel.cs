﻿namespace Fortis.Domain.Assets
{
    public class AssetReadModel
    {
        // ----- Properties
        public string Code { get; set; }
        public string TownName { get; set; }
        public string RegionName { get; set; }
        public float? X { get; set; }
        public float? Y { get; set; }
    }
}