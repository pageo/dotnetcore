﻿using System.Collections.Generic;
using Fortis.Domain.WorkingFolders;

namespace Fortis.Domain.Assets
{
    public interface IAssetRepository
    {
        // Read model
        IReadOnlyCollection<AssetReadModel> Get(WorkingFolder workingFolder, int? take = null, int? skip = null);
    }
}
