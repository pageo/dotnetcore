﻿using System.Collections.Generic;
using Fortis.Domain.WorkingFolders;
using Fortis.Domain.WorkingFolders.Exceptions;

namespace Fortis.Domain.Assets
{
    public class GetAssetByWorkingFolderIdQueryHandler : IHandler<GetAssetByWorkingFolderId, IEnumerable<AssetReadModel>>
    {
        // ----- Fields
        private readonly IWorkingFolderRepository _workingFolderRepository;
        private readonly IAssetRepository _assetRepository;

        // ----- Constructors
        public GetAssetByWorkingFolderIdQueryHandler(
            IWorkingFolderRepository workingFolderRepository,
            IAssetRepository assetRepository)
        {
            _workingFolderRepository = workingFolderRepository;
            _assetRepository = assetRepository;
        }

        // ----- Public methods
        public IEnumerable<AssetReadModel> Handle(GetAssetByWorkingFolderId command)
        {
            var workingFolder = _workingFolderRepository.Get(command.WorkingFolderId);
            if (workingFolder == null)
                throw new WorkingFolderNotFound(command.WorkingFolderId);

            var assets = _assetRepository.Get(workingFolder, command.Take, command.Skip);

            return assets;
        }
    }
}
