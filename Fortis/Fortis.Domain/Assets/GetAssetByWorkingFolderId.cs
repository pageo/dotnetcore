﻿namespace Fortis.Domain.Assets
{
    public class GetAssetByWorkingFolderId
    {
        // ----- Properties
        public int WorkingFolderId { get; set; }
        public int UserId { get; set; }
        public int? Skip { get; set; }
        public int? Take { get; set; }

        // ----- Constructors
        public GetAssetByWorkingFolderId(int workingFolderId, int userId, int? skip, int? take)
        {
            WorkingFolderId = workingFolderId;
            UserId = userId;
            Skip = skip;
            Take = take;
        }
    }
}