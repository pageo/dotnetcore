﻿namespace Fortis.Domain.Animes
{
    public class AnimeReadModel
    {
        // ----- Properties
        public string ImageUrl { get; set; }
        public string Name { get; set; }
        public string PageId { get; set; }
    }
}