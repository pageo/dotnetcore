﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using HtmlAgilityPack;

namespace Fortis.Domain.Animes
{
    public class GetAnimesCommandHandler : IHandler<GetAnimesCommand, IEnumerable<AnimeReadModel>>
    {
        // ----- Fields
        public HttpClient HttpClient => new HttpClient { BaseAddress = new Uri("https://9anime.to/") };

        // ----- Public methods
        public IEnumerable<AnimeReadModel> Handle(GetAnimesCommand message)
        {
            var response = HttpClient.GetStringAsync("").Result;
            return ParseAnimesOnList(response);
        }

        // ----- Internal logics
        private static IEnumerable<AnimeReadModel> ParseAnimesOnList(string response)
        {
            var htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(response);

            return
                htmlDocument.DocumentNode
                            .SelectSingleNode("//div[@class='list-film']")
                            .SelectSingleNode("//div[@data-name='updated' and @class='content']")
                            .SelectNodes("//div[@class='item']")
                            .Select(
                                item => {
                                    var img = item.Descendants("img").First();
                                    var a = item.Descendants("a").Last();

                                    var srcValue = img.Attributes["src"].Value;

                                    return new AnimeReadModel {
                                        ImageUrl = srcValue.Substring(srcValue.LastIndexOf("http:", StringComparison.Ordinal)),
                                        Name = a.InnerText,
                                        PageId = a.Attributes["href"].Value.Replace("https://9anime.to/watch/", "")
                                    };
                                }).ToList();
        }
    }
}
