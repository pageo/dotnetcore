﻿namespace Fortis.Domain
{
    public interface IHandler<in TMessage, out TResult> where TMessage : class
    {
        TResult Handle(TMessage message);
    }

    public interface IHandler<in TMessage>
    {
        void Handle(TMessage message);
    }
}