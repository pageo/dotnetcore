﻿using System;

namespace Fortis.Domain
{
    public interface IAppLogger
    {
        void Log(Exception exception);
        void LogInfo(string message);
        void LogWarning(string message);
    }
}