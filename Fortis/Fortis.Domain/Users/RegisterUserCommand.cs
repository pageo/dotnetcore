﻿using System.ComponentModel.DataAnnotations;

namespace Fortis.Domain.Users
{
    public class RegisterUserCommand
    {
        // ----- Propeeties
        [Required]
        public string FullName { get; }

        [Required]
        public string LoginOrEmail { get; }

        [Required]
        public string Password { get; }

        // ----- Constructors
        public RegisterUserCommand(string fullName, string loginOrEmail, string password)
        {
            FullName = fullName;
            LoginOrEmail = loginOrEmail;
            Password = password;
        }
    }
}