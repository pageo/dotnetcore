﻿namespace Fortis.Domain.Users
{
    public class GetUserCommand
    {
        // ----- Fields
        public int UserId { get; }

        // ----- Constructors
        public GetUserCommand(int userId)
        {
            UserId = userId;
        }
    }
}
