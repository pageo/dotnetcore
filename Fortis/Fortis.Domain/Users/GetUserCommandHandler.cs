﻿namespace Fortis.Domain.Users
{
    public class GetUserCommandHandler : IHandler<GetUserCommand, User>
    {
        // ----- Fields
        private readonly IUserRepository _userRepository;

        // ----- Constructors
        public GetUserCommandHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        // ----- Public methods
        public User Handle(GetUserCommand command)
        {
            return _userRepository.GetUser(command.UserId);
        }
    }
}
