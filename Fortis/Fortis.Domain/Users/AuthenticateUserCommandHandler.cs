using Fortis.Domain.Users.Exceptions;

namespace Fortis.Domain.Users
{
    public class AuthenticateUserCommandHandler : IHandler<AuthenticateUserCommand, int>
    {
        // ----- Fields
        private readonly IUserRepository _userRepository;
        private readonly IClock _clock;

        // ----- Constructors
        public AuthenticateUserCommandHandler(IUserRepository userRepository, IClock clock)
        {
            _userRepository = userRepository;
            _clock = clock;
        }

        // ----- Public methods
        public int Handle(AuthenticateUserCommand command)
        {
            var user = _userRepository.GetUser(command.LoginOrEmail);
            if (user == null) throw new UserNotFound(command.LoginOrEmail);
            _userRepository.UpdateLastLoginDate(user.Id, _clock.Now());
            return user.Id;
        }
    }
}