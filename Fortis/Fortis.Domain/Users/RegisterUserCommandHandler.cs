﻿using Fortis.Domain.Users.Exceptions;

namespace Fortis.Domain.Users
{
    public class RegisterUserCommandHandler : IHandler<RegisterUserCommand, int>
    {
        // ----- Fields
        private readonly IUserRepository _userRepository;

        // ----- Constructors
        public RegisterUserCommandHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        // ----- Public methods
        public int Handle(RegisterUserCommand command)
        {
            if (_userRepository.Exists(command.LoginOrEmail)) throw new UserWithSameNameAlreadyExists(command.LoginOrEmail);
            var newUser = new User {
                Login = !command.LoginOrEmail.Contains("@") ? command.LoginOrEmail : command.LoginOrEmail.Split('@')[0],
                Email = command.LoginOrEmail.Contains("@") ? command.LoginOrEmail : null,
                Password = command.Password,
                FullName = command.FullName
            };
            return _userRepository.Create(newUser);
        }
    }
}