﻿using System.ComponentModel.DataAnnotations;

namespace Fortis.Domain.Users
{
    public class AuthenticateUserCommand
    {
        // ----- Properties
        [Required]
        public string LoginOrEmail { get; }

        [Required]
        public string Password { get; }

        // ----- Constructors
        public AuthenticateUserCommand(string loginOrEmail, string password)
        {
            LoginOrEmail = loginOrEmail;
            Password = password;
        }
    }
}