﻿using System;

namespace Fortis.Domain.Users
{
    public interface IUserRepository
    {
        User GetUser(string loginOrEmail);
        User GetUser(int userId);
        bool Exists(int userId);
        bool Exists(string loginOrEmail);
        int Create(User user);
        void UpdateLastLoginDate(int userId, DateTime lastLoginDate);
    }
}