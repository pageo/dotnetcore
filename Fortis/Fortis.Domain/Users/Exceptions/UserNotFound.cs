﻿using System;

namespace Fortis.Domain.Users.Exceptions
{
    public class UserNotFound : Exception
    {
        // ----- Cosntructors
        public UserNotFound(int userId) : base(BuildMessage(userId))
        {
        }
        public UserNotFound(string loginOrEmail) : base(BuildMessage(loginOrEmail))
        {
        }

        // ----- Internal logics
        private static string BuildMessage(int userId)
        {
            return $"The user with id '{userId}' was not found.";
        }
        private static string BuildMessage(string loginOrEmail)
        {
            return $"The user with login '{loginOrEmail}' was not found.";
        }
    }
}