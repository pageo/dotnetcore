﻿using System;

namespace Fortis.Domain.Users.Exceptions
{
    public class UserWithSameNameAlreadyExists : Exception
    {
        // ----- Cosntructors
        public UserWithSameNameAlreadyExists(string loginOrEmail) : base(BuildMessage(loginOrEmail))
        {
        }

        // ----- Internal logics
        private static string BuildMessage(string loginOrEmail)
        {
            return $"The user with login/email '{loginOrEmail}' already exist.";
        }
    }
}
