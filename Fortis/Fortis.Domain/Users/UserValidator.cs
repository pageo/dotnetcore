﻿using Fortis.Domain.Validators;

namespace Fortis.Domain.Users
{
    public class UserValidator : IValidator<User>
    {
        // ----- Public methods
        public bool Validate(User model)
        {
            return model.Login != null;
        }
    }
}