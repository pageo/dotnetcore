﻿using System;

namespace Fortis.Domain.Users
{
    public class User
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public string Avatar { get; set; }
        public DateTime LastLoginDate { get; set; }
        public bool IsAdmin { get; set; }
    }
}