﻿using System;

namespace Fortis.Domain
{
    public interface IClock
    {
        DateTime Now();
    }
}