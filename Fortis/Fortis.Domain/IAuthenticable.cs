﻿namespace Fortis.Domain
{
    public interface IAuthenticable
    {
        int UserId { get; }
    }
}
